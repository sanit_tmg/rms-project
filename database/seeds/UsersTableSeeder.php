<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'     => 'Abin Rimal',
            'address'  =>  'Kathmandu,Nepal',
            'email'    => 'abin@cinqsnipe.com',
            'password' => Hash::make('awesome'),
            'role'  =>1,
            'usertype' => 'sa'
        ]);
    }
}