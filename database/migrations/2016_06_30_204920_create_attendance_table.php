<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance',function(Blueprint $table){
            $table->increments('id');
            $table->integer('student_id');
            $table->integer('class_id');
            $table->date('date');
            $table->tinyInteger('status');
            $table->string('leave_type');
            $table->string('description');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attendance');
    }
}
