<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
            $table->string('address')->after('name');
            $table->string('phone')->after('address');
            $table->smallInteger('role')->comment('1=>admin;0=>users')->after('remember_token');
            $table->string('usertype')->after('role');
            $table->string('permission')->after('usertype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){

            $table->dropColumn('role');
            $table->dropColumn('usertype');

        });
    }
}
