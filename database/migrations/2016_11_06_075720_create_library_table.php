<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library',function(Blueprint $table){
            $table->increments('id');
            $table->integer('book_no');
            $table->integer('isbn_no');
            $table->Text('book_title');
            $table->Text('book_type');
            $table->string('author_name');
            $table->string('quantity');
            $table->Text('edition');
            $table->string('price');
            $table->Text('publisher_name');
            $table->DATE('added_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('library');
    }
}
