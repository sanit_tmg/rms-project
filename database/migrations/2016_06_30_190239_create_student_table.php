<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student',function(Blueprint $table){
            $table->increments('id');
            $table->integer('roll_no');
            $table->string('name');
            $table->integer('class_id');
            $table->string('address');
            $table->string('email');
            $table->string('parents_name');
            $table->string('parents_phone');
            $table->string('student_phone');
            $table->string('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student');
    }
}
