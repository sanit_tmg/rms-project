<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results',function(Blueprint $table){
            $table->increments('id');
            $table->integer('student_id');
            $table->integer('exam_id');
            $table->longText('marks');
            $table->string('total');
            $table->string('percentage');
            $table->string('grade');
            $table->string('result_date');
            $table->string('result_status');
            $table->longText('remarks');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('results');
    }
}
