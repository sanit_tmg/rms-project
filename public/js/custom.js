/**
 * Created by Abin on 7/10/2016.
 */
$(function () {
    //bootstrap WYSIHTML5 - text editor
    //$(".textarea").wysihtml5();
    //$("#compose-textarea").wysihtml5();
});
$('.marks').change(function(e){
    total();
});
var $check = [];



function final(res){
    $('#result-sts').attr("value","pass");
    for(var j = 0;j<res.length;j++){
        if(res[j] == "fail"){
            $('#result-sts').attr("value","fail");
            continue;
        }

    }

}
function total(){
    $check = [];
    var marks = $('.marks');
    for(var i = 0;i<marks.length;i++){
        var status = marks.eq(i).val();
        if(status == undefined){
            status = 0;
        }
        if( status >= 32){
            $check[i] = "pass";
            $check.push($check[i]);
        }
        else{
            $check[i] = "fail";
            $check.push($check[i]);
        }


    }
    final($check);

    var arr = document.getElementsByClassName('marks');
    var tot = 0;
    var per = 0;
    for(var i=0;i<arr.length;i++){
        if(parseInt(arr[i].value))
            tot += parseInt(arr[i].value);
    }
    document.getElementById('total').value = tot;


    var full = document.getElementsByClassName('full-marks');
    var total = 0;
    for(var j=0;j<full.length;j++){
        if(parseInt(full[j].value))
            total += parseInt(full[j].value);
    }


    per = (tot / total) * 100;
    document.getElementById('percent').value = per;


    if(per>=90){
        document.getElementById('grade').value = 'A+';
    }
    else if(per>=80){
        document.getElementById('grade').value = 'A';
    }
    else if(per>=70){
        document.getElementById('grade').value = 'B+';
    }
    else if(per>=60){
        document.getElementById('grade').value = 'B';
    }
    else if(per>=50){
        document.getElementById('grade').value = 'C+';
    }
    else if(per>=40){
        document.getElementById('grade').value = 'C';
    }
    else if(per>=30){
        document.getElementById('grade').value = 'D+';
    }
    else{
        document.getElementById('grade').value = 'D';
    }



    if(per>=90){
        document.getElementById('gpa').value = '4';
    }
    else if(per>=80){
        document.getElementById('gpa').value = '3.6';
    }
    else if(per>=70){
        document.getElementById('gpa').value = '3.2';
    }
    else if(per>=60){
        document.getElementById('gpa').value = '2.8';
    }
    else if(per>=50){
        document.getElementById('gpa').value = '2.4';
    }
    else if(per>=40){
        document.getElementById('gpa').value = '2';
    }
    else if(per>=30){
        document.getElementById('gpa').value = '1.6';
    }
    else{
        document.getElementById('gpa').value = '1.2';
    }



    var a = document.getElementsByClassName('marks').length;
    for(b=0; b<=a; b++)
    {
        var num = parseFloat($('#marks'+b).val());
        if(num <= 30 ){
            $('#sub-grade'+b).val('D');
        }
        else if(num < 40){
            $('#sub-grade'+b).val('D+');
        }
        else if(num < 50){
            $('#sub-grade'+b).val('C');
        }
        else if(num < 60){
            $('#sub-grade'+b).val('C+');
        }
        else if(num < 70){
            $('#sub-grade'+b).val('B');
        }
        else if(num < 80){
            $('#sub-grade'+b).val('B+');
        }
        else if(num < 90){
            $('#sub-grade'+b).val('A');
        }
        else{
            $('#sub-grade'+b).val('A+');
        }
    }


}

