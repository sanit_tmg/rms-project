$(document).ready( function () {
    $('#dataTables-example').dataTable();

    $('.dataTables-example').dataTable();

    $('.dateSelect input').datepicker({
        format: "yyyy-mm-dd",
        weekStart: 0,
        todayBtn: "linked",
        autoclose:true
        //daysOfWeekDisabled: "6",
        //endDate: "today"
    });
	
});
// Sales chart
var line;
function createMorrisChart(dataObj){
    if(typeof Morris=='object' && typeof dataObj=='object'){
        line = new Morris.Line({
            element: 'revenue-chart',
            resize: true,
            data: dataObj,
            xkey: 'days',
            ykeys: ['bookings'],
            labels: ['Bookings'],
            lineColors: ['#a0d0e0'],
            parseTime: false,
            hideHover: 'auto'
        });
    }
}