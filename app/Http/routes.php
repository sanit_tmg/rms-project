<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middlewareGroups' => ['web']], function () {
    Route::get('login', array('uses' => 'HomeController@index', 'as' => 'admin.login'));

    Route::get('/', 'HomeController@index');
    Route::post('/login-attempt', 'HomeController@postLogin');

    Route::get('logout', array('uses' => 'HomeController@showLogout'));

    Route::group(['middleware' => 'auth'], function () {

        Route::get('/dashboard', array('uses' => 'HomeController@showIndex',
            'as' => 'admin.dashboard'));

        Route::get('/settings', array('uses' => 'HomeController@showUsers',
            'as' => 'admin.setting'));

        Route::get('/settings/company/{id}', array('uses' => 'CompanyController@edit',
            'as' => 'admin.setting.company'));

        Route::post('/update', array('uses' => 'CompanyController@update',
            'as' => 'update.company'));

        Route::get('/changePassword', array('uses' => 'HomeController@changePassword',
            'as' => 'admin.change.password.form'));
        Route::post('/changePassword/setting', array('uses' => 'HomeController@UpdatePassword',
            'as' => 'admin.change.password'));
        Route::get('/addUser', array('uses' => 'HomeController@AddUser',
            'as' => 'admin.AddUser'));
        Route::post('/storeUser', array('uses' => 'HomeController@StoreUser',
            'as' => 'admin.StoreUser'));
        Route::get('/deleteUser/{id}', array('uses' => 'HomeController@Destroy',
            'as' => 'admin.DeleteUser'));
        Route::get('/editUser/{id}', array('uses' => 'HomeController@edit',
            'as' => 'admin.EditUser'));
        Route::post('/editUser/{id}', array('uses' => 'HomeController@update',
            'as' => 'admin.UpdateUser'));


        /*Student Management routes*/
        Route::get('Student/students', array('uses' => 'StudentController@ViewIndex',
            'as' => 'admin.students'));
        Route::get('Student/AddStudent', array('uses' => 'StudentController@create',
            'as' => 'admin.AddStudent'));
        Route::post('Student/Create', array('uses' => 'StudentController@store',
            'as' => 'admin.CreateStudent'));
        Route::get('Student/edit/{id}', array('uses' => 'StudentController@edit',
            'as' => 'admin.EditStudent'));
        Route::post('Student/update/{id}', array('uses' => 'StudentController@update',
            'as' => 'admin.UpdateStudent'));
        Route::get('Student/destroy/{id}', array('uses' => 'StudentController@destroy',
            'as' => 'admin.DeleteStudent'));

        /*Class Management routes*/
        Route::get('Class/classes', array('uses' => 'ClassController@ViewIndex',
            'as' => 'admin.classes'));
        Route::get('Class/AddClass', array('uses' => 'ClassController@create',
            'as' => 'admin.AddClass'));
        Route::post('Class/Create', array('uses' => 'ClassController@store',
            'as' => 'admin.CreateClass'));
        Route::get('Class/edit/{id}', array('uses' => 'ClassController@edit',
            'as' => 'admin.EditClass'));
        Route::post('Class/update/{id}', array('uses' => 'ClassController@update',
            'as' => 'admin.UpdateClass'));
        Route::get('Class/destroy/{id}', array('uses' => 'ClassController@destroy',
            'as' => 'admin.DeleteClass'));

        /*Subject Management routes*/
        Route::get('Subject/subjects', array('uses' => 'SubjectController@ViewIndex',
            'as' => 'admin.Subjects'));
        Route::get('Subject/AddSubject', array('uses' => 'SubjectController@create',
            'as' => 'admin.AddSubject'));
        Route::post('Subject/Create', array('uses' => 'SubjectController@store',
            'as' => 'admin.CreateSubject'));
        Route::get('Subject/edit/{id}', array('uses' => 'SubjectController@edit',
            'as' => 'admin.EditSubject'));
        Route::post('Subject/update/{id}', array('uses' => 'SubjectController@update',
            'as' => 'admin.UpdateSubject'));
        Route::get('Subject/destroy/{id}', array('uses' => 'SubjectController@destroy',
            'as' => 'admin.DeleteSubject'));

        /*Exam Management routes*/
        Route::get('Exam/exams', array('uses' => 'ExamController@ViewIndex',
            'as' => 'admin.Exams'));
        Route::get('Exam/AddExam', array('uses' => 'ExamController@create',
            'as' => 'admin.AddExam'));
        Route::post('Exam/Create', array('uses' => 'ExamController@store',
            'as' => 'admin.CreateExam'));
        Route::get('Exam/edit/{id}', array('uses' => 'ExamController@edit',
            'as' => 'admin.EditExam'));
        Route::post('Exam/update/{id}', array('uses' => 'ExamController@update',
            'as' => 'admin.UpdateExam'));
        Route::get('Exam/destroy/{id}', array('uses' => 'ExamController@destroy',
            'as' => 'admin.DeleteExam'));


        /*Result Management routes*/
        Route::get('Result/results', array('uses' => 'ResultController@ViewIndex',
            'as' => 'admin.Results'));
        Route::get('Result/AddResult', array('uses' => 'ResultController@create',
            'as' => 'admin.AddResult'));
        Route::get('Result/ClassExams/{id}', array('uses' => 'ResultController@classExams',
            'as' => 'admin.CreateClassResult'));
        Route::get('Result/CreateResult/exam/{exam}/class/{class}', array('uses' => 'ResultController@ResultForm',
            'as' => 'admin.ResultForm'));
        Route::post('Result/Create', array('uses' => 'ResultController@store',
            'as' => 'admin.CreateResult'));
        Route::get('Result/ClassResult/{id}', array('uses' => 'ResultController@classResult',
            'as' => 'admin.ClassResult'));
        Route::get('Result/ResultList/exam/{exam}/class/{class}', array('uses' => 'ResultController@ResultList',
            'as' => 'admin.ResultList'));
        Route::get('Result/EmailResult/exam/{exam}/class/{class}', array('uses' => 'ResultController@ResultMail',
            'as' => 'admin.EmailResult'));
        Route::get('Result/ResultSheet/{id}', array('uses' => 'ResultController@ResultSheet',
            'as' => 'admin.ResultSheet'));
        Route::get('Result/ResultMail/{id}', array('uses' => 'ResultController@ResultMail',
            'as' => 'admin.ResultMail'));
        Route::get('Result/EditResult/{id}', array('uses' => 'ResultController@edit',
            'as' => 'admin.EditResult'));
        Route::get('Result/DeleteResult/{id}', array('uses' => 'ResultController@destroy',
            'as' => 'admin.DeleteResult'));
        Route::get('Result/DeleteResult/{id}', array('uses' => 'ResultController@destroy',
            'as' => 'admin.DeleteResult'));


        /*Attendance Management Routes*/
        Route::get('Attendance/class', array('uses' => 'AttendanceController@index',
            'as' => 'attendance'));
        Route::get('Attendance/ClassAttendance/{id}', array('uses' => 'AttendanceController@showIndex',
            'as' => 'ClassAttendance'));
        Route::post('Attendance/StudentAttendance/{id}', array('uses' => 'AttendanceController@store',
            'as' => 'StudentAttendance'));
        Route::get('Attendance/Classes', array('uses' => 'AttendanceController@classes',
            'as' => 'admin.attendance'));
        Route::get('Attendance/ViewDate/{id}', array('uses' => 'AttendanceController@getDate',
            'as' => 'AttendanceDate'));
        Route::get('Attendance/ViewAttendance/{id}', array('uses' => 'AttendanceController@show',
            'as' => 'AttendanceView'));

        /*Emails Management routes*/
        Route::get('Result/SendMail', array('uses' => 'EmailController@SendMail',
            'as' => 'admin.SendMail'));
        Route::post('Result/ComposeMail', array('uses' => 'EmailController@Compose',
            'as' => 'admin.compose'));
        Route::get('Result/MailTemplate/{id}', array('uses' => 'EmailController@MailTemplate',
            'as' => 'admin.TemplateMail'));
        Route::get('Result/MailBox', array('uses' => 'EmailController@MailBox',
            'as' => 'admin.MailBox'));
        Route::post('Result/Email', array('uses' => 'EmailController@Email',
            'as' => 'admin.email'));
        Route::get('Result/EditEmail/{id}', array('uses' => 'EmailController@edit',
            'as' => 'admin.EditEmail'));
        Route::post('Result/EditTemplate/{id}', array('uses' => 'EmailController@update',
            'as' => 'admin.EditTemplate'));
        Route::get('Result/DeleteEmail/{id}', array('uses' => 'EmailController@Destroy',
            'as' => 'admin.DeleteEmail'));

        /*SMS Management routes*/
        Route::get('Result/SendSMS', array('uses' => 'SMSController@SendSMS',
            'as' => 'admin.SendSMS'));
        Route::post('Result/ComposeSMS', array('uses' => 'SMSController@Compose',
            'as' => 'admin.ComposeSMS'));
        Route::get('Result/SMSTemplate/{id}', array('uses' => 'SMSController@MailTemplate',
            'as' => 'admin.TemplateSMS'));
        Route::get('Result/SMSDrafts', array('uses' => 'SMSController@MailBox',
            'as' => 'admin.SMSBox'));
        Route::get('Result/EditDraft/{id}', array('uses' => 'SMSController@edit',
            'as' => 'admin.EditDraft'));
        Route::post('Result/EditDraftTemplate/{id}', array('uses' => 'SMSController@update',
            'as' => 'admin.EditDraftTemplate'));
        Route::get('Result/DeleteSMS/{id}', array('uses' => 'SMSController@Destroy',
            'as' => 'admin.DeleteSMS'));

        /*LIBRARY management system*/
        Route::get('library/lib', array('uses' => 'LibraryController@ViewIndex',
            'as' => 'admin.library'));
        Route::post('Library/Create', array('uses' => 'LibraryController@store',
            'as' => 'admin.CreateLibrary'));
        /*end of library management system*/
        Route::get('AllResult/exam/{exam}/class/{class}', array('uses' => 'ResultController@allresult',
            'as' => 'allResult'));

    });

});