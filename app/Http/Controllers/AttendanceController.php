<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Attendance;
use DB;
use Illuminate\Support\Facades\Mail;

class AttendanceController extends Controller
{
    private $attend;
    private $mai;
    public function __construct(Attendance $attend)
    {
        $this->attend = $attend;
    }

    public function index(){
        $class=DB::table('class')->select('id','class', 'section')->get();
        return view('attendance.list_class',['class'=>$class]);

    }

    public function showIndex($id){
        $attend=DB::table('attendance')
            ->where('class_id',$id)
            ->where('date',date('Y-m-d'))->get();
        if($attend)
        {
            return redirect()->route('admin.dashboard')->with('flash_success', 'Attendance Already Taken Today For This Class!!');
        }
        else{
             $student = DB::table('student')
                ->join('class', 'student.class_id', '=', 'class.id')
                ->select('class.*', 'student.*')
                ->where('student.class_id',$id)
                ->get();
            return view('attendance.list_student',['students'=>$student]);
        }
    }
    public function store(Request $request,$id)
    {
        $student=DB::table('student')->select('id','class_id','name','email')
            ->where('class_id',$id)->get();
        $count = count($student);
        for($i=0;$i<$count;$i++)
        {
            if(is_null($request->input('status'.$i))) {
                $status = 'Absent';

                $this->mai = $student[$i]->email;
                $data=[
                    'subject'=>'absent Notification',
                    'mes'=>'This is to inform that you were absent in class today'
                ];
                Mail::send('email.message', $data, function ($messag) {
                    $messag->from('satcirclenepal@gmail.com', 'SATNP');
                    $messag->to($this->mai,'SATNP')->subject('School Mail');
                });
            }
            else{
                $status = $request->input('status' . $i);
            }
            $data=[
                'student_id'=>$request->input('student'.$i),
                'class_id'=>$id,
                'date'=>date('Y-m-d'),
                'status'=>$status
            ];

            $attendance = $this->attend->create($data);
        }
        return redirect()->route('admin.dashboard')->with('flash_success', 'Attendance taken Successfully!!');;
    }
    public function classes(){
        $class=DB::table('class')->select('id','class', 'section')->get();
        return view('attendance.view_class',['class'=>$class]);
    }
    public function getDate($id)
    {
        return view('attendance.select_date',['classid'=>$id]);
    }
    public function getAttendanceByRange($class, $from, $to)
    {
        /* Formatting from date */
        $from = new \DateTime($from);
        $from = $from->format('Y-m-d');

        /* Formatting to date */
        $to = new \DateTime($to);
        $daysInMonth = cal_days_in_month(0, $to->format('m'), $to->format('Y'));
        $to = $to->modify("+" . ($daysInMonth - 1) . " days")->format('Y-m-d');

        return \DB::table('attendance')
            ->join('student', 'student.id', '=', 'attendance.student_id')
            ->select('attendance.*', 'student.*')
            ->where('attendance.class_id', '=', $class)
            ->whereBetween('attendance.date', [$from, $to])
            ->get();
    }
    public function show(Request $request,$id)
    {

        $class=$id;
        if ($request->exists('from') || $request->exists('to')) {
            $from = $request->get('from');
            $to = $request->get('to');
            $data = $this->getAttendanceByRange($class, $from, $to);
            if ($data) {
                return view('attendance.list_attendance', ['data' => $data, 'from' => $from, 'to' => $to]);
            } else {
                return redirect()->route('AttendanceView',$class)->with('flash_success', 'Attendance Not Taken In this Months ');
            }
        }else {
            $data = DB::table('attendance')
                ->join('student', 'student.id', '=', 'attendance.student_id')
                ->select('attendance.*', 'student.*')
                ->where('attendance.class_id', $class)
                ->get();
            $from = date('Y M');
            $to = date('Y M');
            if ($data) {
                return view('attendance.list_attendance', ['data' => $data, 'from' => $from, 'to' => $to]);
            } else {
                return redirect()->route('admin.dashboard')->with('flash_success', 'Attendance Not Taken For This Class ');
            }
        }

    }
}
