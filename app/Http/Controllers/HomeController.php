<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    private $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function index()
    {
        return view('auth/login')->with('flash_error','how r u doing?');
    }
    public function postLogin(Request $request){
        $email = $request->get('username');
        $password =$request->get('password');
        $remember = $request->get('remember_me');
        if (Auth::attempt(['email' => $email, 'password' => $password],$remember)) {
            return redirect()->intended('dashboard')->with('flash_notice', 'You have successfully logged in');
        }
        return redirect()->back()->with('flash_error', 'Invalid Username/Password!');
    }

    public function showLogout()
    {
        Auth::logout(); // log the user out of our application
        return redirect()->route('admin.login')->with('flash_notice', 'You are successfully logged out');; // redirect the user to the login screen
    }

    public function showIndex(){
        return view('home');
    }
    public function changePassword(){
        $email = Auth::user()->email;
        $id = Auth::user()->id;
        return view('ChangePassword', ['email' => $email, 'id' => $id]);
    }
    public function UpdatePassword(Request $request){
        $this->validate($request, [
            'newPassword' => 'required',
            'confirmNewPassword' => 'required',
        ]);
        if ($request->input('newPassword') == $request->input('confirmNewPassword')) {
            $id = $request->input('id');

            $password = password_hash($request->input('newPassword'), PASSWORD_DEFAULT);
            $result = DB::table('users')->where('id', $id)->update(['password' => $password]);
            if ($result) {
                return redirect('/changePassword')->with('msg', 'Password Changed Successfully');
            } else {
                return redirect('/changePassword')->with('msg', 'Password Changing Failed');
            }
        } else {
            return redirect('/changePassword')->with('msg', 'Password Mismatched');
        }

    }
    public function showUsers(){
        $setting=DB::table('company')->get();

        return view('settings.settings',['users'=>$this->user->all(),'setting'=>$setting]);
    }
    public function AddUser(){
        return view('AddUser');
    }
    public function StoreUser(Request $request){

        $data=[
            'name'=>$request->input('fullname'),
            'address'=>$request->input('address'),
            'phone'=>$request->input('phone'),
            'email'=>$request->input('email'),
            'password'=>password_hash($request->input('password'), PASSWORD_DEFAULT),
            'role'=>$request->input('usertype'),
        ];
        $user = $this->user->create($data);
        if(is_null($user))
        {
            return abort(404);
        }
        return redirect()->route('admin.setting')
            ->with('flash_notice', 'New User Added')
            ->with('status', 'success');


    }
    public function edit($id){
        $user = $this->user->find($id);
        if(is_null($user))
        {
            return abort(404);
        }
        return view('EditUser',['user'=>$user]);
    }
    public function update(Request $request,$id)
    {
        $user = $this->user->find($id);
        if(is_null($user))
        {
            return abort(404);
        }
        $data=[
            'name'=>$request->input('fullname'),
            'address'=>$request->input('address'),
            'phone'=>$request->input('phone'),
            'email'=>$request->input('email'),
            'password'=>password_hash($request->input('password'), PASSWORD_DEFAULT),
            'role'=>$request->input('usertype'),
        ];
        $user->fill($data)->save();
        return redirect()->route('admin.setting')
            ->with('flash_notice', 'User Updated')
            ->with('status', 'success');
    }
    public function Destroy($id){

        $user = $this->user->find($id);
        if(is_null($user))
        {
            return abort(404);
        }
        $user->delete();
        return redirect()->route('admin.setting')
            ->with('flash_notice', 'User Deleted')
            ->with('status', 'success');

    }

}
