<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Email;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    private $email;
    private $mai;
    public function __construct(Email $email)
    {
        $this->email = $email;
    }
    public function  SendMail(){
        return View('email.SendMail',['mails'=>$this->email->all()]);
    }
    public function MailBox(){

        return view('email.Mailbox',['mails'=>$this->email->all()]);
    }
    public function Compose(Request $request)
    {
        if(is_null($request->input('draft')))
        {
            $read = $request->input();
            $student = DB::table('student')->get();
            return view('email.List',['students'=>$student,'message'=>$read]);
        }
        else{
            $subject=$request->input('subject');
            $message=$request->input('message');
            $data=[
                'subject'=>$subject,
                'message'=>$message
            ];
            $email = $this->email->create($data);
            return redirect()->route('admin.SendMail')->with('flash_success', 'Message Saved As Template');
        }

    }
    public function MailTemplate($id)
    {
        $mail = $this->email->find($id);
        $read = [
            'subject'=>$mail->subject,
            'message'=>$mail->message
        ];
        $student = DB::table('student')->get();
        return view('email.List',['students'=>$student,'message'=>$read]);

    }
    public function Email(Request $request)
    {
        $student=DB::table('student')->get();
        $count = count($student);
        $subject=$request->input('subject');
        $mes=$request->input('message');

        for($i=0;$i<=$count;$i++)
        {
            if($request->input('status'.$i)) {
                $data=[
                    'email'=>$student[$i]->email,
                    'subject'=>$subject,
                    'mes'=>$mes
                ];
                $this->mai = $student[$i]->email;
                Mail::send('email.message', $data, function ($messag) {
                    $messag->from('satcirclenepal@gmail.com', 'SAT Circle');
                    $messag->to($this->mai,'SAT Circle')->subject('School Mail');
                });
            }

        }
        return redirect()->route('admin.SendMail')->with('flash_success', 'Email Sent Successfully!!');;
    }
    public function Destroy($id)
    {
        $email = $this->email->find($id);
        if(is_null($email))
        {
            return abort(404);
        }
        $email->delete();
        return redirect()->route('admin.MailBox')->with('flash_success', 'Mail Deleted Successfully!!')->with('status', 'success');
    }
    public function edit($id){
        $email = $this->email->find($id);
        if(is_null($email))
        {
            return abort(404);
        }
        return view('email.template',['email'=>$email]);
    }
    public function update(Request $request,$id)
    {
        $email = $this->email->find($id);
        if(is_null($email))
        {
            return abort(404);
        }
        $data=[
            'id'=>$id,
            'subject'=>$request->input('subject'),
            'message'=>$request->input('message'),
        ];
        $email->fill($data)->save();
        return redirect()->route('admin.MailBox')->with('flash_success', 'Mail Updated Successfully!!')->with('status', 'success');

    }

}
