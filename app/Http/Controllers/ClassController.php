<?php

namespace App\Http\Controllers;

use App\Models\ClassModel;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use App\Http\Requests;

class ClassController extends Controller
{
    private $class;
    public function __construct(ClassModel $class)
    {
        $this->class = $class;
    }

    public function ViewIndex(){
       /* dd($this->class->all());*/
        return view('AvailableClasses',['classes'=>$this->class->all()]);
    }
    public function create(){
        return view('CreateClass');
    }

    public  function store(Request $request)
    {
        $data=[
            'class'=>$request->input('className'),
            'section'=>$request->input('section'),
        ];
        $class = $this->class->create($data);
        if(is_null($class))
        {
            return abort(404);
        }
        return redirect()->route('admin.classes')
            ->with('flash_notice', 'New Class Created.')
            ->with('status', 'success');
    }
    public function edit($id){
        $class = $this->class->find($id);
        if(is_null($class))
        {
            return abort(404);
        }
        return view('UpdateClass',['class'=>$class]);
    }

    public function update(Request $request,$id)
    {
        $class = $this->class->find($id);
        if(is_null($class))
        {
            return abort(404);
        }
        $data=[
            'id'=>$id,
            'class'=>$request->input('className'),
            'section'=>$request->input('section'),
        ];
        $class->fill($data)->save();
        return redirect()->route('admin.classes')
            ->with('flash_notice', 'Class Updated')
            ->with('status', 'success');
    }

    public function destroy($id){
        $class = $this->class->find($id);
        if(is_null($class))
        {
            return abort(404);
        }
        $class->delete();
        return redirect()->route('admin.classes')
            ->with('flash_notice', 'Class Deleted')
            ->with('status', 'success');

    }
}
