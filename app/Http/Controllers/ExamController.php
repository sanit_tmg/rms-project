<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exam;
use App\Http\Requests;

class ExamController extends Controller
{
    private $exam;
    public function __construct(Exam $exam)
    {
        $this->exam = $exam;
    }
    public function ViewIndex(){
        return view('ListExams',['exams'=>$this->exam->all()]);
    }
    public function create(){
        return view('CreateExam');
    }
    public  function store(Request $request)
    {
        $data=[
            'name'=>$request->input('exam_name'),
            'exam_date'=>$request->input('exam_date'),
            'description'=>$request->input('description'),
        ];
        $exam = $this->exam->create($data);
        if(is_null($exam))
        {
            return abort(404);
        }
        return redirect()->route('admin.Exams')
            ->with('flash_notice', 'New Exam Created.')
            ->with('status', 'success');
    }
    public function edit($id){
        $exam = $this->exam->find($id);
        if(is_null($exam))
        {
            return abort(404);
        }
        return view('UpdateExam',['exam'=>$exam]);
    }

    public function update(Request $request,$id)
    {
        $exam = $this->exam->find($id);
        if(is_null($exam))
        {
            return abort(404);
        }
        $data=[
            'name'=>$request->input('exam_name'),
            'exam_date'=>$request->input('exam_date'),
            'description'=>$request->input('description'),
        ];
        $exam->fill($data)->save();
        return redirect()->route('admin.Exams')
            ->with('flash_notice', 'Class Updated')
            ->with('status', 'success');
    }

    public function destroy($id){
        $exam = $this->exam->find($id);
        if(is_null($exam))
        {
            return abort(404);
        }
        $exam->delete();
        return redirect()->route('admin.Exams')
            ->with('flash_notice', 'Class Deleted')
            ->with('status', 'success');

    }
}
