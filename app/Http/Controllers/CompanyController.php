<?php
namespace App\Http\Controllers;
use App\Models\company;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class CompanyController extends Controller
{
    private $comp;

    public function __construct(company $comp_us)
    {
        $this->comp = $comp_us;
    }
    public function edit($did)
    {

        $id = base64_decode($did);
        $comp = $this->comp->find($id);
        if(is_null($comp))
        {
            return 'null';
        }
        return view('settings.settingedit',['data'=>$comp]);
    }

    public function update(Request $request)
    {
        $company = $this->comp->find($request->input('id'));

        if(is_null($company))
        {
            return abort(404);
        }
        if(is_null($request->file('logo')))
        {
            $data=[
                'name' => $request->input('name'),
                'address' => $request->input('address'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'regd' => $request->input('regd'),
                'vat' => $request->input('vat'),
            ];
            $company->fill($data)->save();
            return redirect()->route('admin.setting')
                ->with('flash_notice', 'client Updated')
                ->with('status', 'success');
        }
else
 {
        $image = "";
        $image =DB::table('company')->get();
        if($image[0]->logo!='null')
        {
            unlink(public_path('images/'.$image[0]->logo));
        }
        if ($request->File('logo')) {
            $file = $request->file('logo');
           // dd($file);
            $image = rand(000, 999) . 'company.' . $file->getClientOriginalExtension();//file ko name ra extension tanne kam garcha
            $destination = public_path('images/');
            $file->move($destination, $image);
        } else {
            //return "no image selected";
        }
        $data=[
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'regd' => $request->input('regd'),
            'vat' => $request->input('vat'),
            'logo' => $image,

        ];
        $company->fill($data)->save();
        return redirect()->route('admin.setting')
            ->with('flash_notice', 'client Updated')
            ->with('status', 'success');
    }
 }
}
