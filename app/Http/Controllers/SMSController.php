<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SMS;
use App\Http\Requests;
use DB;

class SMSController extends Controller
{
    private $phone;
    private $message;
    public function __construct(SMS $message)
    {
        $this->message = $message;
    }
    public function  SendSMS(){
        return View('sms.SendSMS',['mails'=>$this->message->all()]);
    }
    public function Compose(Request $request)
    {
        if(is_null($request->input('draft')))
        {
            $read = $request->input();
            $student = DB::table('student')->get();
            return view('sms.List',['students'=>$student,'message'=>$read]);
        }
        else{
            $message=$request->input('message');
            $data=[
                'title'=>'Draft on '.date('Y/m/d'),
                'message'=>$message
            ];
            $email = $this->message->create($data);
            return redirect()->route('admin.SendSMS')->with('flash_success', 'Message Saved As Template');
        }

    }
    public function MailTemplate($id)
    {
        $mail = $this->message->find($id);
        $read = [
            'title'=>$mail->subject,
            'message'=>$mail->message
        ];
        $student = DB::table('student')->get();
        return view('sms.List',['students'=>$student,'message'=>$read]);

    }
    public function MailBox(){

        return view('sms.Mailbox',['mails'=>$this->message->all()]);
    }
    public function Destroy($id)
    {
        $message = $this->message->find($id);
        if(is_null($message))
        {
            return abort(404);
        }
        $message->delete();
        return redirect()->route('admin.SMSBox')->with('flash_success', 'Message Deleted Successfully!!')->with('status', 'success');
    }
    public function edit($id){
        $message = $this->message->find($id);
        if(is_null($message))
        {
            return abort(404);
        }
        return view('sms.template',['email'=>$message]);
    }
    public function update(Request $request,$id)
    {
        $message = $this->message->find($id);
        if(is_null($message))
        {
            return abort(404);
        }
        $data=[
            'id'=>$id,
            'title'=>$request->input('subject'),
            'message'=>$request->input('message'),
        ];
        $message->fill($data)->save();
        return redirect()->route('admin.SMSBox')->with('flash_success', 'Mail Updated Successfully!!')->with('status', 'success');

    }

}
