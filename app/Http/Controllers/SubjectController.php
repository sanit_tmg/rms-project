<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;
use App\Http\Requests;
use DB;

class SubjectController extends Controller
{
    private $subject;
    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
    }
    public function ViewIndex(){
        $data = DB::table('subject')
            ->join('class','class.id','=','subject.class_id')
            ->select('class.*', 'subject.*')
            ->get();
        return view('ListSubjects',['subjects'=>$data]);
    }
    public function create(){
        $result=DB::table('class')->select('id','class')->get();
        return view('CreateSubject',['class'=>$result]);
    }
    public  function store(Request $request)
    {
        $data=[
            'class_id'=>$request->input('class'),
            'subjects'=>$request->input('subject'),
            'full_mark'=>$request->input('full_mark'),
            'pass_mark'=>$request->input('pass_mark'),
            'author'=>$request->input('author'),
        ];
        $subject = $this->subject->create($data);
        if(is_null($subject))
        {
            return abort(404);
        }
        return redirect()->route('admin.Subjects')
            ->with('flash_notice', 'New Subject Created.')
            ->with('status', 'success');
    }
    public function edit($id){
        $subject = $this->subject->find($id);
        if(is_null($subject))
        {
            return abort(404);
        }

        $result=DB::table('class')->select('id','class')->get();
        return view('UpdateSubject',['subjects'=>$subject,'class'=>$result]);
    }
    public function update(Request $request,$id)
    {
        $subject = $this->subject->find($id);
        if(is_null($subject))
        {
            return abort(404);
        }
        $data=[
            'class_id'=>$request->input('class'),
            'subjects'=>$request->input('subject'),
            'full_mark'=>$request->input('full_mark'),
            'pass_mark'=>$request->input('pass_mark'),
            'author'=>$request->input('author'),
        ];
        $subject->fill($data)->save();
        return redirect()->route('admin.Subjects')
            ->with('flash_notice', 'Subjects Updated')
            ->with('status', 'success');
    }
    public function destroy($id){
        $subject = $this->subject->find($id);
        if(is_null($subject))
        {
            return abort(404);
        }
        $subject->delete();
        return redirect()->route('admin.Subjects')
            ->with('flash_notice', 'Subject Deleted')
            ->with('status', 'success');

    }
}
