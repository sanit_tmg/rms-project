<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Result;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use DB;

class ResultController extends Controller
{
    private $result;
    public $mai;
    public static $classId;
    public function __construct(Result $result)
    {
        $this->result = $result;

    }
    public function ViewIndex(){
     /*   dd($this->results->all());*/
        $result=DB::table('class')->select('id','class', 'section')->get();
        return view('ClassResult',['class'=>$result]);
        /*return view('Result Classes');*/
    }
    public function classResult($id){
        $result = DB::table('result')->select('class_id')->where('class_id',$id)->get();
        if($result==null)
        {
            session()->flash('flash_error','Result Not Published');
            return view('ClassWiseResult',['classid'=>$id]);
        }
        $exam=DB::table('exam')->select('id','name', 'description')->get();
        return view('ClassWiseResult',['exam'=>$exam,'class'=>$id]);
    }


    public function ResultList($id,$cid)
    {
        $result = DB::table('result')->select('exam_id')->where('exam_id',$id)->get();
        if($result==null)
        {
            session()->flash('flash_error','Result Not Published For This Examination');
            return view('StudentResult',['examid'=>$id,'classid'=>$cid]);

        }
        $examId=$id;
        $classId=$cid;
        //$student=DB::table('student')->select('id','name','roll_no')->where('id', '=',$studentId)->get();
        $data = DB::table('student')
            ->join('result', 'student.id', '=', 'result.student_id')
            ->select('student.*', 'result.*')
            ->where('result.exam_id', $examId)
            ->where('student.class_id', $classId)
            ->get();
        return view('StudentResult',['results'=>$data]);
    }
    public function ResultSheet($id)
    {
        $resultId=base64_decode($id);
        $data = DB::table('result')
                ->join('student','student.id', '=', 'result.student_id')
                ->select('result.*','student.*')
                ->where('result.id',$resultId)
                ->get();
        $setting=DB::table('company')
            ->select('name','regd','phone','address')
            ->get();


        return view('marksheet',['results'=>$data,'resultid'=>$resultId,'setting'=>$setting]);
    }
    public function ResultMail($id)
    {
        $resultId=$id;
        $results = DB::table('result')
            ->join('student','student.id', '=', 'result.student_id')
            ->select('result.*','student.*')
            ->where('result.id',$resultId)
            ->get();

        $this->mai = $results[0]->email;


        Mail::send('mail.marksheet', (array)$results[0], function ($message) {
            $message->from('satnp@gmail.com', 'Result Marksheet');
            $message->to($this->mai,' School/College Management System')->subject('Marksheet Mail');
        });

        $examId=$results[0]->exam_id;
        $classId=$results[0]->class_id;
       return view('mail.status',['exam'=>$examId,'class'=>$classId]);

    }

    public function create()
    {
        $result=DB::table('class')->select('id','class', 'section')->get();
        return view('createResult',['class'=>$result]);

    }

    public function classExams($id){
        $classId=$id;
       $exam=DB::table('exam')->select('id','name', 'description')->get();
        return view('ClassExams',['exam'=>$exam,'classid'=>$classId]);
    }

    public function ResultForm($id,$cid){
        $classId=$cid;
        $examId=$id;
        $students=DB::table('student')
            ->select('id','name','class_id')
            ->where('class_id','=',$classId)
            ->get();
        /*echo '<pre>';
        var_dump($students);
        echo '<pre/>';*/
        /*$result=DB::table('student')
            ->union('result')
            ->select('student.id','student.name','student.class_id','result.exam_id')

            ->get();
        dd($result);*/
        $subject=DB::table('subject')->select('id','subjects','full_mark','pass_mark')->where('class_id','=',$classId)->get();
        return view('ResultForm',['students'=>$students,'subjects'=>$subject,'classid'=>$classId,'examid'=>$examId]);
    }

    public function store(Request $request){
        $subject=DB::table('subject')->select('id','subjects','full_mark','pass_mark')->where('class_id','=',$request->input('class_id'))->get();
        $countRows= count($subject);
        $j=0;
        for ($i = 0; $i < $countRows; $i++) {
            $Subject=$request->input('subject'.$i);
            $FullMarks=$request->input('FullMarks'.$i);
            $PassMarks=$request->input('PassMarks'.$i);
            $ObtainedMarks=$request->input('ObtainedMarks'.$i);
            $Grade=$request->input('Grade'.$i);
            $marks['Marks'][$j] = array('Subject' => $Subject,
                'FullMarks' => $FullMarks,
                'PassMarks' => $PassMarks,
                'ObtainedMarks' => $ObtainedMarks,
                'Grade' => $Grade);
            $j++;
            }
        $m=json_encode($marks);
        $data=[
            'class_id'=>$request->input('class_id'),
            'student_id'=>$request->input('studentIndex'),
            'exam_id'=>$request->input('exam_id'),
            'marks'=>$m,
            'total'=>$request->input('total'),
            'percentage'=>$request->input('percentage'),
            'grade'=>$request->input('grade'),
            'gpa'=>$request->input('gpa'),
            'result_date'=>$request->input('result_date'),
            'result_status'=>$request->input('result_status'),
            'remarks'=>$request->input('remarks'),
            'is_active'=>$request->input('is_active'),
        ];
        $result = $this->result->create($data);
        if(is_null($result))
        {
            return abort(404);
        }
        return redirect()->route('admin.ResultList',['exam'=>$request->input('exam_id'),'class'=>$request->input('class_id')])
            ->with('flash_notice', 'New Result Added.')
            ->with('status', 'success');
    }

    public function edit($id){
        $result=$this->result->find($id);

        if(is_null($result))
        {
            return abort(404);
        }
        $data = DB::table('result')->select('id','class_id')->where('id','=',$id)->get();

        dd($data[0]->class_id);
        $students=DB::table('student')->select('id','name','class_id')->where('class_id','=',$classId)->get();
        $subject=DB::table('subject')->select('id','subjects','full_mark','pass_mark')->where('class_id','=',$classId)->get();

        dd($result);

    }

    public function destroy($id){

        $result = $this->result->find($id);
        if(is_null($result))
        {
            return abort(404);
        }
        $result->delete();
       return back();

    }

    public function allresult($id,$cid)
    {

        $result = DB::table('result')->select('exam_id')->where('exam_id',$id)->get();
        if($result==null)
        {
            session()->flash('flash_error','Result Not Published For This Examination');
            return view('StudentResult',['examid'=>$id,'classid'=>$cid]);

        }
        $examId=$id;
        $classId=$cid;
        $result = DB::table('result','class')
            ->join('student','student.id', '=', 'result.student_id')
            ->select('result.*','student.*')
            ->where('result.exam_id', $examId)
            ->where('student.class_id', $classId)
            ->get();
        $setting=DB::table('company')
            ->select('name','regd','phone','address')
            ->get();

        $classId=$result[0]->class_id;
        return view('allresult',['results'=>$result,'class'=>$classId,'setting'=>$setting],(compact('result')));

    }
}
