<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Library;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use DB;

class LibraryController extends Controller
{
    private $library;
    public function __construct(Library $library)
    {
        $this->library = $library;
    }

    public function ViewIndex()
    {
        return view('library.create');
    }

    public function store(Request $request)
    {
        $data=[
            'class_id'=>$request->input('class'),
            'subjects'=>$request->input('subject'),
            'full_mark'=>$request->input('full_mark'),
            'pass_mark'=>$request->input('pass_mark'),
            'author'=>$request->input('author'),
        ];
        $library = $this->library->create($data);
        if(is_null($library))
        {
            return abort(404);
        }
        return redirect()->route('admin.Subjects')
            ->with('flash_notice', 'New Subject Created.')
            ->with('status', 'success');
    }
}