<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Http\Requests;
use DB;

class StudentController extends Controller
{
    private $student;
    public function __construct(Student $student)
    {
        $this->student = $student;
    }
    public function ViewIndex(){
        $data = DB::table('student')
            ->join('class', 'student.class_id', '=', 'class.id')
            ->select('class.*', 'student.*')
            ->get();

        return view('student.ListStudents',['students'=>$data]);
    }
    public function create(){
        $result=DB::table('class')->select('id','class', 'section')->get();
        return view('student.CreateStudent',['class'=>$result]);
    }
    public function fileUpload($tmpName, $destinationPath,$imageName){

        $moveimg = $tmpName->move($destinationPath, $imageName);
        if($moveimg){
            return true;
        }
        else{
            return false;
        }
    }
    public  function store(Request $request)
    {

        if(is_null($request->file('image')))
        {
            $imageName='null';
        }
        else {

            $extension = $request->file('image')->getClientOriginalExtension();
            if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'JPG') {
                $tmppath = $request->file('image');
                $imageName = 'student' . rand(0000, 9999) . '.' . 'jpg';
                $path = base_path() . '/public/images/student';
                $img = $this->fileUpload($tmppath, $path, $imageName);
            }
        }
        $class=DB::table('class')->select('id','class', 'section')->get();
        $count=count($class);

        for($i=0;$i<$count;$i++) {
            if ($request->input('class') == $class[$i]->id) {
                    $class_name=$class[$i]->class;
            }
        }
        $data=[
            'roll_no'=>$request->input('roll_no'),
            'name'=>$request->input('name'),
            'class_id'=>$request->input('class'),
            'class'=> $class_name,
            'section'=>$request->input('section'),
            'address'=>$request->input('address'),
            'email'=>$request->input('email'),
            'parents_name'=>$request->input('parents_name'),
            'parents_phone'=>$request->input('parents_phone'),
            'student_phone'=>$request->input('student_phone'),
            'photo'=>$imageName,
        ];
        $student = $this->student->create($data);
        if(is_null($student))
        {
            return abort(404);
        }
        return redirect()->route('admin.students')
            ->with('flash_notice', 'New Student Created.')
            ->with('status', 'success');
    }
    public function edit($id){
        $student = $this->student->find($id);
        if(is_null($student))
        {
            return abort(404);
        }
        $result=DB::table('class')->select('id','class', 'section')->get();
        return view('student.UpdateStudent',['student'=>$student,'class'=>$result]);
    }

    public function update(Request $request,$id)
    {
        $student = $this->student->find($id);
        if(is_null($student))
        {
            return abort(404);
        }
        if(is_null($request->file('image')))
        {
            $class = DB::table('class')->select('id', 'class', 'section')->get();
            $count = count($class);

            for ($i = 0; $i < $count; $i++) {
                if ($request->input('class') == $class[$i]->id) {
                    $class_name = $class[$i]->class;
                }
            }
            $data = [
                'roll_no' => $request->input('roll_no'),
                'name' => $request->input('name'),
                'class_id' => $request->input('class'),
                'class' => $class_name,
                'section' => $request->input('section'),
                'address' => $request->input('address'),
                'email' => $request->input('email'),
                'parents_name' => $request->input('parents_name'),
                'parents_phone' => $request->input('parents_phone'),
                'student_phone' => $request->input('student_phone')
            ];
            $student->fill($data)->save();
            return redirect()->route('admin.students')
                ->with('flash_notice', 'Student Updated')
                ->with('status', 'success');
        }
        else {
            $image =DB::table('student')->select('id', 'photo')->where('id',$id)->get();
            if($image[0]->photo!='null')
            {
                unlink(public_path('images/student/'.$image[0]->photo));
            }
            $extension = $request->file('image')->getClientOriginalExtension();
            if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'JPG') {
                $tmppath = $request->file('image');
                $imageName = 'student' . rand(0000, 9999) . '.' . 'jpg';
                $path = base_path() . '/public/images/student';
                $img = $this->fileUpload($tmppath, $path, $imageName);
            }
            $class = DB::table('class')->select('id', 'class', 'section')->get();
            $count = count($class);

            for ($i = 0; $i < $count; $i++) {
                if ($request->input('class') == $class[$i]->id) {
                    $class_name = $class[$i]->class;
                }
            }
            $data = [
                'roll_no' => $request->input('roll_no'),
                'name' => $request->input('name'),
                'class_id' => $request->input('class'),
                'class' => $class_name,
                'section' => $request->input('section'),
                'address' => $request->input('address'),
                'email' => $request->input('email'),
                'parents_name' => $request->input('parents_name'),
                'parents_phone' => $request->input('parents_phone'),
                'student_phone' => $request->input('student_phone'),
                'photo' => $imageName
            ];
            $student->fill($data)->save();
            return redirect()->route('admin.students')
                ->with('flash_notice', 'Student Updated')
                ->with('status', 'success');
        }
    }

    public function destroy($id){
        $student = $this->student->find($id);
        if(is_null($student))
        {
            return abort(404);
        }
        $image =DB::table('student')->select('id', 'photo')->where('id',$id)->get();
        if($image[0]->photo!='null')
        {
            unlink(public_path('images\student\\'.$image[0]->photo));

        }
        $student->delete();
        return redirect()->route('admin.students')
            ->with('flash_notice', 'Student Deleted')
            ->with('status', 'success');

    }

}
