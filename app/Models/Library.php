<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    protected $table = 'library';
    protected $fillable = ['book_no','isbn_no','book_title','book_type','author_name','quantity','edition','price','publisher_name'];
}
