<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'result';
    protected $fillable = ['class_id','student_id','exam_id','marks','total','percentage','grade','gpa','result_date','result_status','remarks','is_active'];
}
