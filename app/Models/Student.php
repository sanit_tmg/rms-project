<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
    protected $fillable = ['class_id','roll_no','name','class','section','address','email','parents_name','parents_phone','student_phone','photo'];
}
