-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2016 at 06:33 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rms`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
`id` int(10) unsigned NOT NULL,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'Abscent',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `student_id`, `class_id`, `date`, `status`, `created_at`, `updated_at`) VALUES
(36, 8, 1, '2016-08-01', 'Abscent', '2016-08-01 06:45:11', '2016-08-01 06:45:11'),
(37, 9, 1, '2016-08-01', 'Present', '2016-08-01 06:45:12', '2016-08-01 06:45:12'),
(38, 10, 1, '2016-08-01', 'Abscent', '2016-08-01 06:45:12', '2016-08-01 06:45:12'),
(39, 11, 1, '2016-08-01', 'Present', '2016-08-01 06:45:12', '2016-08-01 06:45:12'),
(40, 8, 1, '2016-08-05', 'Present', '2016-08-05 02:06:52', '2016-08-05 02:06:52'),
(41, 9, 1, '2016-08-05', 'Present', '2016-08-05 02:06:52', '2016-08-05 02:06:52'),
(42, 10, 1, '2016-08-05', 'Present', '2016-08-05 02:06:52', '2016-08-05 02:06:52'),
(43, 11, 1, '2016-08-05', 'Abscent', '2016-08-05 02:06:52', '2016-08-05 02:06:52'),
(44, 14, 6, '2016-08-16', 'Present', '2016-08-16 03:44:34', '2016-08-16 03:44:34'),
(71, 8, 1, '2016-08-18', 'Present', '2016-08-18 05:36:49', '2016-08-18 05:36:49'),
(72, 10, 1, '2016-08-18', 'Absent', '2016-08-18 05:36:54', '2016-08-18 05:36:54'),
(73, 11, 1, '2016-08-18', 'Present', '2016-08-18 05:36:55', '2016-08-18 05:36:55'),
(74, 13, 1, '2016-08-18', 'Absent', '2016-08-18 05:37:00', '2016-08-18 05:37:00'),
(75, 16, 1, '2016-08-18', 'Present', '2016-08-18 05:37:00', '2016-08-18 05:37:00'),
(76, 18, 4, '2016-08-18', 'Present', '2016-08-18 07:05:47', '2016-08-18 07:05:47'),
(77, 19, 4, '2016-08-18', 'Absent', '2016-08-18 07:06:37', '2016-08-18 07:06:37'),
(78, 20, 4, '2016-08-18', 'Present', '2016-08-18 07:06:37', '2016-08-18 07:06:37'),
(79, 21, 4, '2016-08-18', 'Absent', '2016-08-18 07:06:48', '2016-08-18 07:06:48');

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
`id` int(10) unsigned NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `class`, `section`, `created_at`, `updated_at`) VALUES
(1, 'One', 'A', NULL, '2016-07-03 22:36:04'),
(4, 'Two', 'A', '2016-07-03 22:36:39', '2016-07-03 22:36:39'),
(6, 'Three', 'A', '2016-07-03 22:37:52', '2016-07-03 22:37:52'),
(9, 'Four', 'A', '2016-07-04 06:00:57', '2016-07-04 06:00:57'),
(12, 'Five', 'A', '2016-07-27 03:16:11', '2016-07-27 03:16:11'),
(13, 'Six', 'A', '2016-07-27 03:16:11', '2016-07-27 03:16:11'),
(14, 'Seven', 'A', '2016-07-27 03:16:11', '2016-07-27 03:16:11'),
(15, 'Eight', 'A', '2016-07-27 03:16:11', '2016-07-27 03:16:11'),
(16, 'Nine', 'A', '2016-07-27 03:16:11', '2016-07-27 03:16:11'),
(17, 'Ten', 'A', '2016-07-27 03:16:11', '2016-07-27 03:16:11');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `regd` varchar(255) DEFAULT NULL,
  `vat` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `address`, `email`, `phone`, `regd`, `vat`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'Cinqsnipe Academy', 'Narayangarh, Chitwan', 'info@cinqsnipe.com', '+977 056 5314442', '07865/67876/99', 'A67G897', 'logo-bw.png', '2016-09-12 13:25:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(11) NOT NULL,
  `subject` text NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(8, 'Teej Programme', '<p>Dear Students,</p>\r\n <p>we are going to held teej program at our school.&nbsp;</p><p>&nbsp;Regards,<br></p><p>Abin Rimal&nbsp;</p><p>Principal</p>', '2016-08-24 12:10:45', '2016-08-24 06:25:45'),
(10, 'Winter Vacations', '<h1><u>Heading Of Message</u></h1>                      <h4>Subheading</h4>                      <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain                          was born and I will give you a complete account of the system, and expound the actual teachings                          of the great explorer of the truth, the master-builder of human happiness. No one rejects,                          dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know                          how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again                          is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,                          but because occasionally circumstances occur in which toil and pain can procure him some great                          pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,                          except to obtain some advantage from it? But who has any right to find fault with a man who                          chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that                          produces no resultant pleasure? On the other hand, we denounce with righteous indignation and                          dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so                          blinded by desire, that they cannot foresee</p>                      <ul>                          <li>List item one</li>                          <li>List item two</li>                          <li>List item three</li>                          <li>List item four</li>                      </ul>                      <p>Thank you,</p>                      <p>John Doe</p>', '2016-08-24 08:55:57', '2016-08-24 03:07:16'),
(11, 'College Holiday', '<h1><u>Heading Of Message</u></h1>\r\n                      <h4>Subheading</h4>\r\n                      <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\r\n                          was born and I will give you a complete account of the system, and expound the actual teachings\r\n                          of the great explorer of the truth, the master-builder of human happiness. No one rejects,\r\n                          dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know\r\n                          how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again\r\n                          is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,\r\n                          but because occasionally circumstances occur in which toil and pain can procure him some great\r\n                          pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,\r\n                          except to obtain some advantage from it? But who has any right to find fault with a man who\r\n                          chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that\r\n                          produces no resultant pleasure? On the other hand, we denounce with righteous indignation and\r\n                          dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so\r\n                          blinded by desire, that they cannot foresee</p>\r\n                      <ul>\r\n                          <li>List item one</li>\r\n                          <li>List item two</li>\r\n                          <li>List item three</li>\r\n                          <li>List item four</li>\r\n                      </ul>\r\n                      <p>Thank you,</p>\r\n                      <p>John Doe</p>', '2016-08-24 06:18:19', '2016-08-24 06:18:19'),
(12, 'Maths', '<p>This is a demo template</p>', '2016-08-24 06:26:43', '2016-08-24 06:26:43');

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE IF NOT EXISTS `exam` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exam_date` date NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`id`, `name`, `exam_date`, `description`, `created_at`, `updated_at`) VALUES
(5, 'First Terminal Exam', '2016-07-31', '<p>\r\n\r\nThis is to inform all those students who have not yet filled in their examination forms till today the last date for submission of online examination application forms for the Semester End Examination November 2012. The online mode of submitting examination forms will close tomorrow i.e. <strong>18.10.2012 at 12.00 mid night.</strong>&nbsp;Thereafter no examination forms shall be entertained online. After 18.10.2012- mid night the students are required to contact the office <strong>(Mr. Mukesh Agarwal / Mr. Navneet Jain)</strong>&nbsp;for filling of examination forms and any query related to examination form.\r\n\r\n<br></p>', '2016-07-27 08:11:41', '2016-07-27 08:11:41'),
(6, 'Second Terminal Exam', '2016-07-31', '<p>\r\n\r\nThis is to inform all those students who have not yet filled in their examination forms till today the last date for submission of online examination application forms for the Semester End Examination November 2012. The online mode of submitting examination forms will close tomorrow i.e. <strong>18.10.2012 at 12.00 mid night.</strong>&nbsp;Thereafter no examination forms shall be entertained online. After 18.10.2012- mid night the students are required to contact the office <strong>(Mr. Mukesh Agarwal / Mr. Navneet Jain)</strong>&nbsp;for filling of examination forms and any query related to examination form.\r\n\r\n<br></p>', '2016-07-27 08:11:41', '2016-07-27 08:11:41'),
(7, 'Third Terminal Exam', '2016-07-31', '<p>\r\n\r\nThis is to inform all those students who have not yet filled in their examination forms till today the last date for submission of online examination application forms for the Semester End Examination November 2012. The online mode of submitting examination forms will close tomorrow i.e. <strong>18.10.2012 at 12.00 mid night.</strong>&nbsp;Thereafter no examination forms shall be entertained online. After 18.10.2012- mid night the students are required to contact the office <strong>(Mr. Mukesh Agarwal / Mr. Navneet Jain)</strong>&nbsp;for filling of examination forms and any query related to examination form.\r\n\r\n<br></p>', '2016-07-27 08:11:41', '2016-07-27 08:11:41'),
(8, 'Final Terminal Exam', '2016-07-31', '<p>\r\n\r\nThis is to inform all those students who have not yet filled in their examination forms till today the last date for submission of online examination application forms for the Semester End Examination November 2012. The online mode of submitting examination forms will close tomorrow i.e. <strong>18.10.2012 at 12.00 mid night.</strong>&nbsp;Thereafter no examination forms shall be entertained online. After 18.10.2012- mid night the students are required to contact the office <strong>(Mr. Mukesh Agarwal / Mr. Navneet Jain)</strong>&nbsp;for filling of examination forms and any query related to examination form.\r\n\r\n<br></p>', '2016-07-27 08:11:41', '2016-07-27 08:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_20_173326_add_fields_to_users_table', 1),
('2016_06_30_190239_create_student_table', 1),
('2016_06_30_191714_create_class_table', 1),
('2016_06_30_194930_create_examination_table', 1),
('2016_06_30_203156_create_subject_table', 1),
('2016_06_30_203829_create_result_table', 1),
('2016_06_30_204920_create_attendance_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
`id` int(10) unsigned NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `exam_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `marks` longtext COLLATE utf8_unicode_ci NOT NULL,
  `total` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percentage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gpa` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `result_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `result_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `class_id`, `student_id`, `exam_id`, `marks`, `total`, `percentage`, `grade`, `gpa`, `result_date`, `result_status`, `remarks`, `is_active`, `created_at`, `updated_at`) VALUES
(8, 1, 10, '5', '{"Marks":[{"Subject":"Maths","FullMarks":"100","PassMarks":"40","ObtainedMarks":"90","Grade":"A+"},{"Subject":"English","FullMarks":"100","PassMarks":"40","ObtainedMarks":"70","Grade":"B+"},{"Subject":"Science","FullMarks":"100","PassMarks":"40","ObtainedMarks":"80","Grade":"A"},{"Subject":"Nepali","FullMarks":"100","PassMarks":"40","ObtainedMarks":"70","Grade":"B+"},{"Subject":"Computer","FullMarks":"100","PassMarks":"40","ObtainedMarks":"90","Grade":"A+"},{"Subject":"Health Science","FullMarks":"100","PassMarks":"40","ObtainedMarks":"90","Grade":"A+"},{"Subject":"General Knowledge","FullMarks":"100","PassMarks":"40","ObtainedMarks":"70","Grade":"B+"},{"Subject":"Grammer","FullMarks":"100","PassMarks":"40","ObtainedMarks":"90","Grade":"A+"}]}', '650', '81.25', 'A', '3.6', '2016-07-31', 'Pass', '<h3><b>lorem Ipsum Dolar Sit Amet</b></h3><p><b><br></b></p>', 1, '2016-07-28 02:50:05', '2016-07-28 02:50:05'),
(9, 1, 8, '5', '{"Marks":[{"Subject":"Maths","FullMarks":"100","PassMarks":"40","ObtainedMarks":"90","Grade":"A+"},{"Subject":"English","FullMarks":"100","PassMarks":"40","ObtainedMarks":"80","Grade":"A"},{"Subject":"Science","FullMarks":"100","PassMarks":"40","ObtainedMarks":"70","Grade":"B+"},{"Subject":"Nepali","FullMarks":"100","PassMarks":"40","ObtainedMarks":"60","Grade":"B"},{"Subject":"Computer","FullMarks":"100","PassMarks":"40","ObtainedMarks":"40","Grade":"C"},{"Subject":"Health Science","FullMarks":"100","PassMarks":"40","ObtainedMarks":"60","Grade":"B"},{"Subject":"General Knowledge","FullMarks":"100","PassMarks":"40","ObtainedMarks":"70","Grade":"B+"},{"Subject":"Grammer","FullMarks":"100","PassMarks":"40","ObtainedMarks":"60","Grade":"B"}]}', '530', '66.25', 'B', '2.8', '2016-07-31', 'Pass', '<p></p><pre><b>admin.ResultList This is demo</b> </pre><p></p>', 1, '2016-07-28 02:56:27', '2016-07-28 02:56:27'),
(13, 6, 14, '5', '{"Marks":[{"Subject":"Maths","FullMarks":"100","PassMarks":"50","ObtainedMarks":"80","Grade":"A"},{"Subject":"Science","FullMarks":"100","PassMarks":"50","ObtainedMarks":"60","Grade":"B"}]}', '140', '70', 'B+', '3.2', '2016-08-16', 'Pass', '<p>&nbsp;This is a tst</p>', 1, '2016-08-16 03:46:57', '2016-08-16 03:46:57'),
(14, 4, 18, '5', '{"Marks":[{"Subject":"Maths","FullMarks":"100","PassMarks":"50","ObtainedMarks":"90","Grade":"A+"},{"Subject":"English","FullMarks":"100","PassMarks":"40","ObtainedMarks":"80","Grade":"A"},{"Subject":"Science","FullMarks":"100","PassMarks":"80","ObtainedMarks":"70","Grade":"B+"}]}', '240', '80', 'A', '3.6', '2016-08-18', 'Pass', '<p>&nbsp;Good Boy</p>', 1, '2016-08-18 03:52:34', '2016-08-18 03:52:34'),
(15, 4, 19, '5', '{"Marks":[{"Subject":"Maths","FullMarks":"100","PassMarks":"50","ObtainedMarks":"90","Grade":"A+"},{"Subject":"English","FullMarks":"100","PassMarks":"40","ObtainedMarks":"90","Grade":"A+"},{"Subject":"Science","FullMarks":"100","PassMarks":"80","ObtainedMarks":"90","Grade":"A+"}]}', '270', '90', 'A+', '4', '2016-08-19', 'Pass', '<p>&nbsp;this is a demo</p>', 1, '2016-08-18 05:24:25', '2016-08-18 05:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE IF NOT EXISTS `sms` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`id`, `title`, `message`, `updated_at`, `created_at`) VALUES
(2, 'This is a Custom Draft', 'Dear Students\r\nHello', '2016-09-07 06:24:49', '2016-09-07 06:13:09'),
(3, 'This is a  custom drtaft', 'dasdsadsadsadsadasdbasjdbsadaga\r\nsdfg\r\nds\r\naf\r\nds\r\nfa\r\ndsaf\r\na\r\nfasdfdsafgbsduhfbisafibsdnajfdsjbnfsdbfkjbsdakfbajkfbdsbfdsbfhb\r\nfdsafasdfsda\r\nfsdafasdfdsafdsfdsffdfdfdfdf ', '2016-09-12 04:15:19', '2016-09-12 04:14:55'),
(4, 'This is a title', 'School will be closed on sunday', '2016-09-15 01:00:37', '2016-09-15 01:00:15');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
`id` int(10) unsigned NOT NULL,
  `class_id` int(11) NOT NULL,
  `roll_no` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parents_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parents_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `student_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `class_id`, `roll_no`, `name`, `class`, `section`, `address`, `email`, `parents_name`, `parents_phone`, `student_phone`, `photo`, `created_at`, `updated_at`) VALUES
(8, 1, 1, 'Namus Jung Adhikari', 'One', 'A', 'Basundhara,Kathmandu', 'namusjung@gmail.com', 'ramesh', '98456321992', '98456321992', 'student3972.jpg', '2016-07-27 05:16:23', '2016-08-18 03:40:48'),
(10, 1, 3, 'Abin Rimal', 'One', 'A', 'Basundhara,Kathmandu', 'busyabin@gmail.com', 'Abin Rimal', '9845431044', '9845431044', 'student9756.jpg', '2016-07-27 05:46:49', '2016-08-18 03:41:18'),
(11, 1, 4, 'Suman', 'One', 'A', 'New Road', 'deadbouy@gmail.com', 'Abin Rimal', '9845431099', '9845431099', 'student334.jpg', '2016-07-27 05:53:43', '2016-08-18 03:41:32'),
(13, 1, 5, 'Sakshyam', 'One', 'A', 'Basundhara,Kathmandu', 'abin.rimal06@gmail.com', 'Abin Rimal', '9845431044', '9845431044', 'student6605.jpg', '2016-07-27 09:29:48', '2016-08-18 03:41:39'),
(16, 1, 2, 'Beeshal Aryal', 'One', 'A', 'Kshetrapur 2', 'beeshal@gmail.com', 'Beeshal', '9845431044', '9845431044', 'student4956.jpg', '2016-08-18 01:35:11', '2016-08-18 03:41:11'),
(18, 4, 1, 'Aman', 'Two', 'A', 'New Road', 'aman@gmail.com', 'Arush', '9845431044', '9845431044', 'student859.jpg', '2016-08-18 03:51:14', '2016-08-18 03:51:14'),
(19, 4, 2, 'Raman', 'Two', 'A', 'Basundhara,Kathmandu', 'busyabin@gmail.com', 'Abin Rimal', '9845431044', '9845431044', 'student1384.jpg', '2016-08-18 03:51:49', '2016-08-18 05:22:05'),
(20, 4, 3, 'Subodh', 'Two', 'A', 'Basundhara,Kathmandu', 'nil_chalise@yahoo.com', 'Nilmodi ', '9845431044', '9845431044', 'student6993.jpg', '2016-08-18 05:21:02', '2016-08-18 05:21:02'),
(21, 4, 4, 'Samridth', 'Two', 'A', 'Basundhara,Kathmandu', 'busyabin@gmail.com', 'Abin Rimal', '9845431044', '9845431044', 'null', '2016-08-18 05:41:58', '2016-08-18 05:42:22'),
(22, 6, 1, 'admin', 'Three', 'A', 'Basundhara,Kathmandu', 'busyabin@gmail.com', 'Abin Rimal', '9845431044', '9845431044', 'student584.jpg', '2016-08-18 07:16:32', '2016-08-18 07:16:32');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
`id` int(10) unsigned NOT NULL,
  `class_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subjects` longtext COLLATE utf8_unicode_ci NOT NULL,
  `full_mark` int(11) NOT NULL,
  `pass_mark` int(11) NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `class_id`, `subjects`, `full_mark`, `pass_mark`, `author`, `created_at`, `updated_at`) VALUES
(10, '1', 'Maths', 100, 40, 'Abin Rimal', '2016-07-27 06:27:58', '2016-07-27 06:27:58'),
(11, '1', 'English', 100, 40, 'Sabin', '2016-07-27 06:28:13', '2016-07-27 06:28:13'),
(12, '1', 'Science', 100, 40, 'Rabin', '2016-07-27 06:28:31', '2016-07-27 06:28:31'),
(13, '1', 'Nepali', 100, 40, 'Sabina', '2016-07-27 06:28:48', '2016-07-27 06:28:48'),
(14, '1', 'Computer', 100, 40, 'Suman Jung  Adhikari', '2016-07-27 06:29:08', '2016-07-27 06:36:02'),
(15, '1', 'Health Science', 100, 40, 'Dr. Bipin', '2016-07-27 06:29:42', '2016-07-27 06:29:42'),
(16, '1', 'General Knowledge', 100, 40, 'Bishal Chhetri', '2016-07-27 06:30:45', '2016-07-28 01:32:37'),
(17, '1', 'Grammer', 100, 40, 'Suyesh Shrestha', '2016-07-27 06:31:22', '2016-07-27 06:35:36'),
(25, '4', 'Maths', 100, 50, 'Dr. Bipin', '2016-08-18 03:48:44', '2016-08-18 03:48:44'),
(26, '4', 'English', 100, 40, 'Abin Rimal', '2016-08-18 03:49:02', '2016-08-18 03:49:02'),
(27, '4', 'Science', 100, 80, 'Rabin Thapa', '2016-08-18 03:49:27', '2016-08-18 03:49:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` smallint(6) NOT NULL COMMENT '1=>admin;0=>users',
  `usertype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `phone`, `email`, `password`, `remember_token`, `role`, `usertype`, `permission`, `created_at`, `updated_at`) VALUES
(1, 'Abin Rimal', 'Kathmandu,Nepal', '+977 9845431044', 'abin@cinqsnipe.com', '$2y$10$0AFdH92jaKAoglgKj84l0emcPiLSOYakJ1kClFc7eNJ3sR7T2PyD.', 'EH3WsI9a9tRAZCroXxkj0DXTRvYVUZFi9ykTEBK5AtPW5rS1ZNV6tNSMtX7q', 1, 'sa', '', NULL, '2016-09-15 01:03:21'),
(3, 'Nil', 'Basundhara,Kathmandu,Nepal', '+977 9856543011', 'nil@rms.com', '$2y$10$zVjQ2muZZ1O9NCJi9/jCouxRqA4/Jal7ugNULskEoDeN3T96uw3ba', 'SVWCK75CDV5mMKaachsSeDnPxO6u6CXSNjenl8oyLIhAC7dtUPCdZK0GSahF', 1, '', '', '2016-07-21 06:05:25', '2016-07-21 06:25:46'),
(4, 'Suraj', 'Gongabu, Kathmandu', '+977 98674545321', 'suraj@gmail.com', '$2y$10$U3SkIt77qssktt8iLcPXz.70ztIIAm6Mh/qdhxDJ8zzMYXQXcorQm', NULL, 1, '', '', '2016-07-21 06:25:42', '2016-07-21 06:25:42'),
(5, 'user', 'user address', '9845431044', 'user@cinqsnipe.com', '$2y$10$QEj3/xjEqEJPLAn9rJ40ruIUoirZhT2rebuRksrFt1fnTbZSjKFAq', 'ejrscKnn9dyvIrPiOtePHfbLX7xQBDfNt5SqA7bgnW920KWwHJJcIJGDM3oW', 0, '', '', '2016-09-12 06:29:19', '2016-09-20 01:06:24'),
(6, 'user1', 'ontario', '9845431044', 'user1@cinqsnipe.com', '$2y$10$iBI80AEXqOjgVRLUtyIsIOJif5J81mRRXiwxQ8tQGtcGEk0Qh7n6q', NULL, 0, '', '', '2016-09-12 06:35:24', '2016-09-12 06:35:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
