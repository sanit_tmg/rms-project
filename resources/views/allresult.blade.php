@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/printcss.css')}}"/>
@endsection
@section('title','All Result','Marksheet')
@section('content')
    @if(Session::has('flash_error'))
        <div class="jumbotron">
            <p class="text-center">
                <span style="color: red; font-weight: bold;"> Result Not Published Yet For This Student </span>
            </p>
            <p class="text-center">
                <a href="#" class="btn btn-info btn-lg ">Add Result Now </a>
            </p>
        </div>
    @else
        <table class="table" id="printableArea">
            @foreach($result as $results)
                <tr>
                    <td colspan="6" style="letter-spacing: 3px; font-weight: bold; font-size: 20px;width:100%; text-align:center; border-top:none; border-bottom: 1px solid #ddd;"><h4>Academic Certificate   Class  {{$results->class}} '{{$results->section}}'<span style="color:blue"></span></h4></td>
                </tr>
                <tr>
                    <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Student Name:</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">{{$results->name}}</td>
                    <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Roll no.</td>
                    <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd;">{{$results->roll_no}}</td>
                    <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Exam Date:</td>
                    <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd;">{{$results->result_date}}</td>
                </tr>
                <tr>
                    <th style="border-top:none; border-bottom: 1px solid #ddd;">Sn.</th>
                    <th style="border-top:none; border-bottom: 1px solid #ddd;">Subject</th>
                    <th style="border-top:none; border-bottom: 1px solid #ddd;">Full Marks</th>
                    <th style="border-top:none; border-bottom: 1px solid #ddd;">Pass Marks</th>
                    <th style="border-top:none; border-bottom: 1px solid #ddd;">Obtained Marks</th>
                    <th style="border-top:none; border-bottom: 1px solid #ddd;">Grade</th>
                </tr>
                <?php
                $count =1;
                $marks= json_decode($results->marks,true);
                for($i=0;$i<=sizeof($marks["Marks"])-1;$i++)
                {
                ?>
                <tr>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;"><?php echo $count; ?></td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["Subject"]}}</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["FullMarks"]}}</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["PassMarks"]}}</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["ObtainedMarks"]}}</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["Grade"]}}</td>
                </tr>
                <?php
                $count++;
                }
                ?>
                <tr>
                    <td style="border-top:none; border-bottom: 1px solid #ddd; padding-left:315px; font-weight:700;" colspan="4">Total</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;" colspan="2">{{$results->total}}</td>
                </tr>
                <tr>
                    <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Percentage</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$results->percentage}}%</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="1">GPA</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;" colspan="1">{{$results->gpa}}</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="1">Grade</td>
                    <td style="border-top:none; border-bottom: 1px solid #ddd;" colspan="1">{{$results->grade}}</td>
                </tr>
                <tr>
                    <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="6">
                        Remarks	:
                        <br/>
                        <p style="font-weight:500;"><?php echo $results->remarks; ?></p>
                    </td>
                </tr>
                <tr>
                    <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="6">
                        principle signature	:
                        <style></style>
                    </td>
                </tr>
            @endforeach
        </table>
        <a class="btn btn-primary" onclick="printable()" href="#" role="button"> <i class="fa fa-print"></i> Print Result</a>
    @endif
@endsection
@section('scripts')
    <script>
        function printable()
        {
            $( "body" ).addClass( "sidebar-collapse" );
            window.print();
            $( "body" ).removeClass( "sidebar-collapse" );
        }
    </script>
@endsection
