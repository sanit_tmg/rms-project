@extends('layouts.app')
@section('title','Send New SMS')
@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <p class="text-center">
            @if (Session::has('flash_success'))
                <span class="fa fa-warning" style="color: red; margin-left: 50px;"></span>
                <span style="color: green;"> {{ Session::get('flash_success') }}</span>
            @else
                {{Session::get('flash_success')}}
            @endif
        </p>
        <div class="col-md-3">
            <a href="{{route('admin.SMSBox')}}" class="btn btn-primary btn-block margin-bottom">Templates Management</a>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-envelope-o"></i> Saved Templates</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        @foreach($mails as $mail)
                            <li><a href="{{route("admin.TemplateSMS",$mail->id)}}">{{$mail->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Compose New Message</h3>
                </div>
                <!-- /.box-header -->
                <form method="post" action="{{route('admin.ComposeSMS')}}">
                    <div class="box-body">
                        {{csrf_field()}}
                        <div class="form-group">
                    <textarea id="compose-textarea" name="message" class="form-control" placeholder="Message Body" style="height: 300px" onKeyDown="limitText();"
                              onKeyUp="limitText();"></textarea>
                        </div>
                        Total Characters: <input type="readonly" id="countdown" name="countdown" value="0" style="border:none; outline: none;" value="0"><span id="sms" style="float: right"></span>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button name="draft" type="submit" class="btn btn-default"><i class="fa fa-pencil"></i>Save As Template</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                        </div>
                        <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
                    </div>
                </form>
                <!-- /.box-footer -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
<script language="javascript" type="text/javascript">
    function limitText(field,count) {
        var count=document.getElementById('countdown');
        count.value = document.getElementById('compose-textarea').value.length;
        switch(count.value)
        {
            case "0":
            {
                document.getElementById('sms').innerHTML=" ";
                break;
            }
            case "161":
            {
                document.getElementById('sms').innerHTML = "Will be sent as 2 SMS";
                break;
            }
            case "321":
            {
                document.getElementById('sms').innerHTML = "Will be sent as 3 SMS";
                break;
            }
            case "481":
            {
                document.getElementById('sms').innerHTML = "Will be sent as 4 SMS";
                break;
            }
            case "541":
            {
                    alert("Maximum Number of characters Reached");
            }

        }


    }
</script>
