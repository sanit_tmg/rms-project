@extends('layouts.app')
@section('title','Edit Draft')
@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Draft</h3>
                </div>
                <!-- /.box-header -->
                <form method="post" action="{{route('admin.EditDraftTemplate',$email->id)}}">
                    <div class="box-body">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input class="form-control" placeholder="Subject:" name="subject" value="{{$email->title}}">
                        </div>
                        <div class="form-group">
                    <textarea id="compose-textarea"  name="message" style="height: 300px" class="form-control" onKeyDown="limitText();"
                              onKeyUp="limitText();">{{$email->message}}</textarea>
                        </div>
                        Total Characters: <input type="readonly" id="countdown" name="countdown" value="0" style="border:none; outline: none;" value="0"><span id="sms" style="float: right"></span>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> Update Draft</button>
                        </div>
                    </div>
                </form>
                <!-- /.box-footer -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
<script language="javascript" type="text/javascript">
    function limitText(field,count) {
        var count=document.getElementById('countdown');
        count.value = document.getElementById('compose-textarea').value.length;
        switch(count.value)
        {
            case "0":
            {
                document.getElementById('sms').innerHTML=" ";
                break;
            }
            case "161":
            {
                document.getElementById('sms').innerHTML = "Will be sent as 2 SMS";
                break;
            }
            case "321":
            {
                document.getElementById('sms').innerHTML = "Will be sent as 3 SMS";
                break;
            }
            case "481":
            {
                document.getElementById('sms').innerHTML = "Will be sent as 4 SMS";
                break;
            }
            case "541":
            {
                alert("Maximum Number of characters Reached");
            }

        }


    }
</script>