@extends('layouts.app')
@section('title','MailBox')
@section('content')
<section class="content-header">
    <h1>
        Mail Templates
        <small>15  Templates</small>
        <span class="col-md-3 pull-right">
             <a href="{{route('admin.SendMail')}}" class="btn btn-primary btn-block margin-bottom">Add New Template</a>
        </span>
    </h1>
    <p class="text-center">
        @if (Session::has('flash_success'))
            <span class="fa fa-warning" style="color: red; margin-left: 50px;"></span>
            <span style="color: green;"> {{ Session::get('flash_success') }}</span>
        @endif
    </p>

</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
                <!-- /.box-body -->
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>s.n</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                $i=1;
                            ?>
                            @foreach($mails as $mail)
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>{{substr($mail->subject,0,80)}}...</td>
                                <td>{{substr($mail->message,0,100)}}...</td>
                                <td><a href="{{route('admin.EditEmail',$mail->id)}}"><i class="fa fa-edit fa-3x"></i></a> <a href="{{route('admin.DeleteEmail',$mail->id)}}"><i class="fa fa-trash fa-3x text-red"></i></a></td>
                            </tr>
                            <?php
                            $i++;
                            ?>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>s.n</th>
                                <th>Subject</th>
                                <th>Date Created</th>
                                <th>Options</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>

@endsection