@extends('layouts.app')
@section('title','Add New Mail Template')
@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Mail Template</h3>
                </div>
                <!-- /.box-header -->
                <form method="post" action="{{route('admin.EditTemplate',$email->id)}}">
                    <div class="box-body">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input class="form-control" placeholder="Subject:" name="subject" value="{{$email->subject}}">
                        </div>
                        <div class="form-group">
                    <textarea id="compose-textarea"  name="message" class="textarea form-control">
                        {{$email->message}}
                    </textarea>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> Update Template</button>
                        </div>
                    </div>
                </form>
                <!-- /.box-footer -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection