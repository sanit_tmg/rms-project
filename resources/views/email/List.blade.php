@extends('layouts.app')
@section('title')
    Select Students to message
@endsection
@section('content')
    <div class="page-two">
        <table class="table">
            <tr>
                <td>Class</td>
                <td>Student Name</td>
                <td>Student Email</td>
                <td>Address</td>
                <td>Guardian Name</td>
                <td><label>
                        <input type="checkbox" id="checkAll" style="display:none;">
                        <strong>Student Select [ Select All ]</strong>
                    </label>
                </td>
            </tr>
            <form action="{{route('admin.email')}}" method="post">
                <input type="hidden" name="subject" value="{{$message['subject']}}">
                <input type="hidden" name="message" value="{{$message['message']}}">
                <?php $i=0; ?>
                {{csrf_field()}}
                @foreach($students as $student)
                    <tr>
                        <td>{{ $student->class }}</td>
                        <input type="hidden" name="student<?php echo $i ?>" value="{{$student->id}}" >
                        <td>{{ $student->name }}</td>
                        <td>{{ $student->email }}</td>

                        <td>{{ $student->address }}</td>
                        <td>{{ $student->parents_name }}</td>
                        <td align="center">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="status<?php echo $i ?>" value="Present" >
                                </label>
                            </div>
                        </td>
                    </tr>
            <?php $i++; ?>
            @endforeach

        </table>
        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send Mail</button>
        </form>



    </div>
    </div><!--page-two ends-->

@endsection
@section('scripts')
    <script type="text/javascript">
        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });
    </script>
@endsection
