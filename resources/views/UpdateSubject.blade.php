@extends('layouts.app')
@section('title','Add New Exam')
@section('content')
    <div class="create-form">
        <form class="form-horizontal" action="{{route('admin.UpdateSubject',$subjects->id)}}" method="post">
            {{csrf_field()}}
            <div class="box-body col-md-offset-1">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="class">Class</label>
                        <select name="class" class="form-control">
                            @foreach($class as $cl)
                                <option value="{{$cl->id}}">{{$cl->class}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-1">
                    <div class="form-group">
                        <label for="name">Subject</label>
                        <input type="text" value="{{$subjects->subjects}}" class="form-control" id="subject" placeholder="Enter Subject" name="subject">
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-1">
                    <div class="form-group">
                        <label for="name">Full Marks</label>
                        <input type="number" value="{{$subjects->full_mark}}" class="form-control" id="full_mark"  name="full_mark">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2 ">
                    <div class="form-group">
                        <label for="name">Pass Marks</label>
                        <input type="number" value="{{$subjects->pass_mark}}" class="form-control" id="pass_mark"  name="pass_mark">
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-1">
                    <div  class="form-group">
                        <label for="author">Author</label>
                        <input type="text" value="{{$subjects->author}}" class="form-control" name="author" placeholder="Author">
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-1" style="padding-top: 20px;">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update Subject</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection