@extends('layouts.app')
@section('title')
   Select Date to View Attendance
@endsection
@section('content')
            <form action="{{route('AttendanceView')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="class_id" value="{{$classid}}">
                <div class="col-md-3 col-md-offset-2">
                    <div class="form-group">
                        <label for="date">Select Date (mm/dd/yyyy)</label>
                        <input type="date" name="date" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="View Now" class="btn btn-primary">
                    </div>
                </div>
            </form>
@endsection
