@extends('layouts.app')
@section('title')
    Students Available is class
@endsection
@section('content')
    <div class="col-md-12">
        @if($students==null)
           <div class="text-center">
               <h2 class="text-bold text-red">Students Not Registered In This Class!!</h2>
               <a href="{{route('attendance')}}" class="btn btn-primary"><i class="fa fa-mail-reply"></i> Return Back</a>
           </div>
            @else
            <table class="table table-responsive">
                <tr>
                    <td>Roll no.</td>
                    <td>Student Name</td>
                    <td class="visible-md visible-sm visible-lg">Address</td>
                    <td class="visible-md visible-sm visible-lg">Guardian Name</td>
                    <td class="visible-md visible-sm visible-lg">Guardian Number</td>
                    <td><div class="checkbox">
                            <label>
                                <input type="checkbox" id="checkAll" style="display:none;">
                                <strong>Student Status [ Select All ]</strong>
                            </label>
                        </div></td>
                </tr>

                <form action="{{route('StudentAttendance',$students[0]->class_id)}}" method="post">
                    <?php $i=0; ?>
                    {{csrf_field()}}
                    @foreach($students as $student)
                <tr>
                    <td>{{ $student->roll_no }}</td>
                    <input type="hidden" name="student<?php echo $i ?>" value="{{$student->id}}" >
                    <td>{{ $student->name }}</td>
                    <td class="visible-md visible-sm visible-lg">{{ $student->address }}</td>
                    <td class="visible-md visible-sm visible-lg">{{ $student->parents_name }}</td>
                    <td class="visible-md visible-sm visible-lg">{{ $student->parents_phone }}</td>
                    <td align="center">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="status<?php echo $i ?>" value="Present" >
                            </label>
                        </div>
                    </td>
                </tr>
                    <?php $i++; ?>
                    @endforeach
            </table>
        <input type="submit" class="btn btn-primary"  value="Place Attendance"/>
        </form>
            @endif
        </div>
    </div><!--page-two ends-->
@endsection
@section('scripts')
    <script type="text/javascript">
    $("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
    </script>
@endsection
