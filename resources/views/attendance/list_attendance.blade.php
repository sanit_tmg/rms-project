@extends('layouts.app')

@section('title')
    <h1>Attendance Sheet For class {{$data[0]->class}} '{{$data[0]->section}}' </h1>
    <a class="btn btn-primary" id="printable" href="#" role="button"> <i class="fa fa-print"></i> Print Result</a>
@endsection
@section('content')
    <style>
        @media print {
            .print {
                display:none
            }
        }
    </style>
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            @if (Session::has('flash_success'))
                <p class="text-center">
                <span class="fa fa-warning fa-2x" style="color: red; margin-left: 50px;"></span>
                <span style="color: red;"> {{ Session::get('flash_success') }}</span>
                </p>
            @endif
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Class {{$data[0]->class}} <span class="small">'{{$data[0]->section}}' </span>
                    <small class="pull-right">Today : {{ date('Y M d') }}</small>
                </h2>
            </div>
            <div class="col-xs-12">
                <form class="form-inline" method="get" >
                    <div class="input-daterange input-group" id="datepicker">
                        <span class="input-group-addon">From</span>
                        <input type="text" value="{{ $from }}" class="input-sm form-control" required="required" name="from" />
                        <span class="input-group-addon">to</span>
                        <input type="text" value="{{ $to }}" class="input-sm form-control" required="required" name="to" />
                    </div>
                    <input type="submit" value="Go" class="btn btn-sm btn-default"/>
                    <hr/>
                </form>
            </div>
            <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th>S.n</th>
                    <th>Roll No</th>
                    <th>Students Name</th>
                    <td>Guardian Name</td>
                    <td>Guardian Number</td>
                    <td>Date</td>
                    <th>Student Status</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
                @foreach($data as $dat)
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>{{$dat->roll_no}}</td>
                        <td>{{$dat->name}}</td>
                        <td>{{$dat->parents_name}}</td>
                        <td>{{substr($dat->parents_phone,0,10)}}</td>
                        <td>{{$dat->date}}</td>
                        <td>{{$dat->status}}</td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
   <script>
      function printable()
        {
            $( "body" ).addClass( "sidebar-collapse" );
            window.print();
            $( "body" ).removeClass( "sidebar-collapse" );
        }

    </script>
    <script>
        $('.input-daterange').datepicker({
        format: "yyyy M",
        minViewMode: 1,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false
        });
        $('#printable').on('click',function(e){
            e.preventDefault();
            $('#example1').DataTable().destroy();
            $('#example1_length,.btn-default').addClass("print");
            $('#example1_filter label').addClass('print');
            $('#example1').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            $( "body" ).addClass( "sidebar-collapse" );
            setTimeout(function(){window.print();},500);


        });
        (function() {

            var afterPrint = function() {
                location.reload();

                $( "body" ).removeClass( "sidebar-collapse" );
            };

            if (window.matchMedia) {
                var mediaQueryList = window.matchMedia('print');
                mediaQueryList.addListener(function(mql) {
                    if (mql.matches) {
                       // beforePrint();
                    } else {
                        afterPrint();
                    }
                });
            }
            window.onafterprint = afterPrint;

        }());
    </script>
@endsection