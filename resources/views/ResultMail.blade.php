@extends('layouts.app')
@section('title')
    Mail Center
@endsection
@section('content')
    <div class="mail text-center">
        @for($i=1;$i<=15;$i++)

            <?php echo '.'; ?>
            <?php time_nanosleep(1,$i); ?>
        @endfor
        <h2 style="color: #00a65a" >Mail Sent Successfully!!</h2>
        <a class="btn btn-default" href="{{route('admin.ResultList',['exam'=>$examId,'class'=>$classId])}}" role="button"><i class="fa fa-mail-reply"></i> Go Back</a>
    </div>
    @endsection