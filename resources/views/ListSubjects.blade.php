@extends('layouts.app')
@section('title')
    Available Subjects <a href="{{route('admin.AddSubject')}}" class="btn btn-default btn-md"> Add New Subject </a>
@endsection
@section('content')
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>S.n</th>
                    <th>Class</th>
                    <th>Subject</th>
                    <th>Pass Mark</th>
                    <th>Full Mark</th>
                    <th>Author</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
              <?php $i=1; ?>
                @foreach($subjects as $sub)
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>{{$sub->class}}</td>
                        <td>{{$sub->subjects}}</td>
                        <td>{{$sub->pass_mark}}</td>
                        <td>{{$sub->full_mark}}</td>
                        <td>{{$sub->author}}</td>
                        <td><a href="{{route("admin.EditSubject", $sub->id)}}}}">Edit</a> |<a href="{{route("admin.DeleteSubject", $sub->id)}}"> Delete</a></td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
