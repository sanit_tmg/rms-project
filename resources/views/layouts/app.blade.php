<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Result Management System </title>
   <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    {{-- Theme--}}
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    {{--Date Picker--}}
    <link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
    {{--Date Range Picker--}}
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker-bs3.css')}}">
    {{--bootstrap wysihtml5 - text editor--}}
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}"/>
    @yield('styles')
</head>
<body class="skin-blue sidebar-mini">
<!--
 BODY TAG OPTIONS:
 =================
 Apply one or more of the following classes to get the
 desired effect
 |---------------------------------------------------------|
 | SKINS         | skin-blue                               |
 |               | skin-black                              |
 |               | skin-purple                             |
 |               | skin-yellow                             |
 |               | skin-red                                |
 |               | skin-green                              |
 |---------------------------------------------------------|
 |LAYOUT OPTIONS | fixed                                   |
 |               | layout-boxed                            |
 |               | layout-top-nav                          |
 |               | sidebar-collapse                        |
 |               | sidebar-mini                            |
 |---------------------------------------------------------|
 -->
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="{{public_path()}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>Management</b>System</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Management</b>System</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{URL::asset('/images/admin.png')}}" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{URL::asset('/images/admin.png')}}" class="img-circle" alt="User Image">

                                <p>
                                    {{ Auth::user()->name }} - Administrator
                                    <small>Member since June. 2016</small>
                                </p>
                            </li>


                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{URL::asset('/images/admin.png')}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">System Menus</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="active"><a href="{{route('admin.dashboard')}}"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>

                <li class="treeview">
                    <a href="{{route('admin.students')}}"><i class="fa fa-users"></i> <span>Student Management</span></a>
                </li>
                <li class="treeview">
                    <a href="{{route('admin.classes')}}"><i class="fa fa-institution"></i> <span>Class Management</span></a>
                </li>
                <li class="treeview">
                    <a href="{{route('admin.Subjects')}}"><i class="fa fa-book"></i> <span>Subjects Management</span></a>
                </li>
                <li class="treeview">
                    <a href="{{route('admin.Exams')}}"><i class="fa fa-legal"></i> <span>Examination Management</span></a>
                </li>

                <li class="treeview">
                    <a href="#"><i class="fa fa-graduation-cap"></i> <span>Library Management</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('admin.library')}}"><i class="fa fa-circle-o-notch"></i>Create Book</a></li>
                        <li><a href=""><i class="fa fa-circle-o-notch"></i>View All Book</a></li>
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#"><i class="fa fa-graduation-cap"></i> <span>Result Management</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('admin.Results')}}"><i class="fa fa-circle-o-notch"></i>View Results</a></li>
                        <li><a href="{{route('admin.AddResult')}}"><i class="fa fa-circle-o-notch"></i>Add Result</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-calendar"></i> <span>Attendance Management</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('attendance')}}"><i class="fa fa-circle-o-notch"></i>Place Attendance</a></li>
                        <li><a href="{{route('admin.attendance')}}"><i class="fa fa-circle-o-notch"></i>View Attendance</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-envelope"></i> <span>Messages Management</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o-notch"></i>Emails Management</a>
                            <ul class="treeview-menu">
                                <li><a href="{{route('admin.SendMail')}}"><i class="fa fa-circle-o-notch"></i>Send New Message</a></li>
                                <li><a href="{{route('admin.MailBox')}}"><i class="fa fa-circle-o-notch"></i>View Messages</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="fa fa-circle-o-notch"></i>SMS Management</a>
                            <ul class="treeview-menu">
                                <li><a href="{{route('admin.SendSMS')}}"><i class="fa fa-circle-o-notch"></i>Send New Message</a></li>
                                <li><a href="{{route('admin.SMSBox')}}"><i class="fa fa-circle-o-notch"></i>View Messages</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="treeview"><a href=""><i class="fa fa-cogs"></i> <span>Settings</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        @if(Auth::user()->role == 1))
                        <li><a href="{{route('admin.setting')}}"><i class="fa fa-circle-o-notch"></i>Main Settings</a></li>
                        @endif
                            <li><a href="{{route('admin.change.password.form')}}"><i class="fa fa-circle-o-notch"></i>Change Password</a></li>

                    </ul>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 900px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('title')
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            @yield('content')


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            www.cinqsnipe.com
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2016 <a href="www.mediawarner.com">Warner Media/ www.mediawarner.com</a>.</strong> All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->

    <!-- JavaScripts -->
    <script src="{{asset('js/jquery.js')}}" ></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/app.min.js')}}"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
       <script src="{{asset('js/jquery-ui.min.js')}}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
{{--Slim Scroll--}}
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
    });
</script>
@yield('scripts')
</body>
</html>
