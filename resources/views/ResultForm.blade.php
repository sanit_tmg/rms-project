@extends('layouts.app')
@section('title')
    Result Form
    @endsection
@section('content')
    <div class="result-form col-md-offset-1">
        <div class="row-fluid">
            <div class="container">
                <form class="form-horizontal" method="post" action="{{route('admin.CreateResult')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="class_id" value="{{$classid}}">
                    <input type="hidden" name="exam_id" value="{{$examid}}">
                    <div class="col-md-4">
                    <div class="form-group date">
                        <label class="control-label" >Result Date (mm/dd/yyyy)</label>
                        <div>
                            <input type="date" name="result_date" class="form-control" >
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group name">
                        <label class="control-label ">Student Name</label>
                        <div  >
                            <select class="form-control" id="name" name="studentIndex">
                                @foreach($students as $student)
                                    <option value="{{$student->id}}">{{$student->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    </div>
                    <?php $j = 0; ?>
                    @foreach($subjects as $sub)
                    <div class="form-group">
                        <div class="subject col-sm-12 col-md-2 col-lg-2">
                            <label>{{$sub->subjects}}
                                <input type="hidden" name="subject<?php echo $j ?>" value="{{$sub->subjects}}">
                            </label>
                        </div>
                        <div class="full-marks col-sm-12 col-md-2 col-lg-2">
                            <label class="control-label" >Full Marks</label>
                            <input type="number" class="form-control full-marks" value="{{$sub->full_mark}}" name="FullMarks<?php echo $j; ?>" id="" readonly >
                        </div>
                        <div class="pass-marks col-sm-12 col-md-2 col-lg-2">
                            <label class="control-label" >Pass Marks</label>
                            <input type="number" class="form-control" value="{{$sub->pass_mark}}" name="PassMarks<?php echo $j; ?>" id="" readonly="">
                        </div>
                        <div class="obtained-marks col-sm col-12-md-2 col-lg-2">
                            <label class="control-label" >Obtained Marks</label>
                            <input type="number" class="form-control marks"  name="ObtainedMarks<?php echo $j; ?>" id="marks<?php echo $j; ?>">
                        </div>
                        <div class="grade col-sm col-12-md-2 col-lg-1">
                            <label class="control-label" >Grade</label>
                            <input type="text/number" class="form-control"  name="Grade<?php echo $j; ?>" id="sub-grade<?php echo $j; ?>" readonly >
                        </div>
                    </div>
                    <?php $j++; ?>
                    @endforeach

                                <!--  -->
                        <div class="form-group total col-sm-12 col-md-2 col-lg-2">
                            <label class=" control-label">Total</label>
                            <div class="">
                                <input type="text" name="total" class="form-control"  id="total" placeholder="Total" readonly="">
                            </div>
                        </div>

                        <div class="form-group percentage col-sm-12 col-md-2 col-lg-2">
                            <label class=" control-label">Percentage</label>
                            <div class="">
                                <input type="text" name="percentage" class="form-control"  id="percent" placeholder="Percentage" readonly="">
                            </div>
                        </div>

                        <div class="form-group total-grade col-sm-12 col-md-2 col-lg-2">
                            <label class=" control-label">Grade</label>
                            <div class="">
                                <input type="text" name="grade" class="form-control"  id="grade" placeholder="Grade" readonly="">
                            </div>
                        </div>

                        <div class="form-group gpa col-sm-12 col-md-2 col-lg-2">
                            <label class=" control-label">GPA</label>
                            <div class="">
                                <input type="text" class="form-control" name="gpa"  id="gpa" placeholder="GPA" readonly="">
                            </div>
                        </div>

                        <div class="result-status gpa col-sm-12 col-md-2 col-lg-2">
                            <label class=" control-label">Result Status</label>
                            <div class="">
                                <input type="text" class="form-control"  id="result-sts" name="result_status" readonly value="Pass">
                            </div>
                        </div>


                        <div class="remarks col-sm-12 col-md-10 col-lg-10">
                            <label class=" control-label">Remarks</label>
                            <div class="">
                                <textarea id="compose-textarea" class="textarea" name="remarks"></textarea>
                            </div>
                        </div>


                        <div class="form-group status">
                            <label class="control-label ">Publish Result</label>
                            <div>
                                <select class="form-control" id="status" name="is_active">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>

                                </select>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <button class="btn btn-default" type="submit">Save & Publish</button>
                </form>
            </div>

        </div>
    </div>

@endsection