@extends('layouts.app')
@section('title','Settings')
@section('content')
    <div class="col-md-4">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Company Info</h3>
                <span class="pull-right">
                    <a href="{{route('admin.setting.company',base64_encode($setting[0]->id))}}" class="btn btn-primary ">Edit Info</a>
                </span>
            </div>
            <div class="box-body company">
                <p>
                    <b>Company Name: </b> <span>{{$setting[0]->name}}</span>
                </p>
                <p>
                    <b>Address : </b> <span>{{$setting[0]->address}}</span>
                </p>
                <p>
                    <b>Email : </b> <span>{{$setting[0]->email}}</span>
                </p>
                <p>
                    <b>Phone Number : </b> <span>{{$setting[0]->phone}}</span>
                </p>
                <p>
                    <b>MOE Regd. Number : </b> <span>{{$setting[0]->regd}}</span>
                </p>
                <p>
                    <b>VAT/PAN Number : </b> <span>{{$setting[0]->vat}}</span>
                </p>
                <p>
                    <span><img src="{{asset('/images/'.$setting[0]->logo)}}" alt=""></span>
                </p>

            </div>

        </div>
    </div>
    <div class="col-md-8">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title">Available Users </h3>
                <span class="pull-right">
                    <a href="{{route('admin.AddUser')}}" class="btn btn-primary">Add New User</a>
                </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>User Type</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                    @foreach($users as $data)
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->address}}</td>
                            <td>{{$data->phone}}</td>
                            <td>{{$data->email}}</td>
                            <td>
                                @if($data->role == 1)
                                    Admin
                                @else
                                    User
                                @endif
                            </td>


                            <td><a href="{{route("admin.EditUser", $data->id)}}"><i class="fa fa-edit"></i></a> |<a href="{{route("admin.DeleteUser", $data->id)}}"> <i class="fa fa-trash"></i></a></td>
                        </tr>
                   <?php $i++; ?>
                    @endforeach

                    </tbody>

                </table>
            </div>
         <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

@endsection