@extends('layouts.app')
@section('title','Edit company Profile')
@section('content')
<br>
    <form class="form-horizontal" action="{{route('update.company')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
        <input type="hidden" name="id" value="{{$data->id}}">
        <div class="form-group">
            <label class="col-sm-3" for="name">Title:</label>
            <div class="col-sm-9">
            <input type="text" name="name" value="{{$data->name}}" class="form-control" data-error="Please enter title." >
            </div>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
            <label class="col-sm-3" for="name">Address:</label>
            <div class="col-sm-9">
            <input type="text" name="address"  value="{{$data->address}}"  class="form-control" data-error="Please enter title." >
            </div>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
            <label class="col-sm-3" for="name">Email:</label>
            <div class="col-sm-9">
            <input type="email" name="email"  value="{{$data->email}}" class="form-control" data-error="Please enter title." >
            </div>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
            <label class="col-sm-3" for="name">Phone:</label>
            <div class="col-sm-9">
            <input type="text" name="phone"  value="{{$data->phone}}"  class="form-control" data-error="Please enter title." >
            </div>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
            <label class="col-sm-3" for="name">MOE Regd. Number:</label>
            <div class="col-sm-9">
            <input type="text" name="regd"  value="{{$data->regd}}"  class="form-control" data-error="Please enter title." >
            </div>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
            <label class="col-sm-3" for="name">VAT/PAN Number:</label>
            <div class="col-sm-9">
            <input type="text" name="vat"  value="{{$data->vat}}" class="form-control" data-error="Please enter title." >
            </div>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
            <img src="{{asset('/images/'.$data->logo)}}"  alt="" style="height: 150px;">
            <input type="file" name="logo"  accept="image/gif, image/jpeg, image/png">
        </div>
        <br>
        <br>
    <div class="group">
        <button type="submit" class="btn btn-info">Update</button>
    </div>
    <!-- /.box-footer -->
</form>


@endsection