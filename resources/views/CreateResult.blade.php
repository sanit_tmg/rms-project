@extends('layouts.app')
@section('title')
    Select class to add result <a href="{{route('admin.AddClass')}}" class="btn btn-default btn-md"> Add New Class </a>
    @endsection
@section('content')
    <div class="col-md-12">
        <div class="row-fluid">
            @foreach($class as $clas)
                <div class="classes col-md-2">
                    <a href="{{route('admin.CreateClassResult', $clas->id)}}">
                        <div>
                            <p><i class="fa fa-users"></i></p>
                            <h4>Class {{ $clas->class }}</h4>
                            <h5>'{{$clas->section}}'</h5>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
