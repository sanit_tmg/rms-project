@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/printcss.css')}}"/>
@endsection
@section('title','Settings','Marksheet')
@section('content')
    @if(Session::has('flash_error'))
        <div class="jumbotron">
            <p class="text-center">
                <span style="color: red; font-weight: bold;"> Result Not Published Yet For This Student </span>
            </p>
            <p class="text-center">
                <a href="#" class="btn btn-info btn-lg ">Add Result Now </a>
            </p>
        </div>
    @else
        <div class="page-two">
            <div class="container">
                {{--<a class="btn btn-success" href="#" role="button" style="margin-bottom:15px;">Back To Class Result</a>--}}
                MOE Regd. Number:  <span>{{$setting[0]->regd}}</span><br>
                <span>{{$setting[0]->address}}</span><br>
                <span>{{$setting[0]->phone}}</span>
                <span><h1 style="text-align: center">{{$setting[0]->name}}</h1></span>
                <table class="table" id="printableArea">
                    <tr>
                        <td colspan="6" style="letter-spacing: 3px; font-weight: bold; font-size: 20px;width:100%; text-align:center; border-top:none; border-bottom: 1px solid #ddd;"><h4>Academic Certificate   Class  {{$results[0]->class}} '{{$results[0]->section}}'<span style="color:blue"></span></h4></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Student Name:</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">{{$results[0]->name}}</td>
                        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Roll no.</td>
                        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd;">{{$results[0]->roll_no}}</td>
                        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Exam Date:</td>
                        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd;">{{$results[0]->result_date}}</td>
                    </tr>
                    <tr>
                        <th style="border-top:none; border-bottom: 1px solid #ddd;">Sn.</th>
                        <th style="border-top:none; border-bottom: 1px solid #ddd;">Subject</th>
                        <th style="border-top:none; border-bottom: 1px solid #ddd;">Full Marks</th>
                        <th style="border-top:none; border-bottom: 1px solid #ddd;">Pass Marks</th>
                        <th style="border-top:none; border-bottom: 1px solid #ddd;">Obtained Marks</th>
                        <th style="border-top:none; border-bottom: 1px solid #ddd;">Grade</th>
                    </tr>
                        <?php
                        $count =1;
                        $marks= json_decode($results[0]->marks,true);
                        for($i=0;$i<=sizeof($marks["Marks"])-1;$i++)
                        {
                        ?>
                    <tr>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;"><?php echo $count; ?></td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["Subject"]}}</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["FullMarks"]}}</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["PassMarks"]}}</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["ObtainedMarks"]}}</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$marks["Marks"][$i]["Grade"]}}</td>
                    </tr>
                        <?php
                            $count++;
                                }
                            ?>
                    <tr>
                        <td style="border-top:none; border-bottom: 1px solid #ddd; padding-left:315px; font-weight:700;" colspan="4">Total</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;" colspan="2">{{$results[0]->total}}</td>
                    </tr>
                    <tr>
                        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Percentage</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$results[0]->percentage}}%</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="1">GPA</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;" colspan="1">{{$results[0]->gpa}}</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="1">Grade</td>
                        <td style="border-top:none; border-bottom: 1px solid #ddd;" colspan="1">{{$results[0]->grade}}</td>
                    </tr>
                    <tr>
                        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="6">
                            Remarks	:
                            <br/>
                            <p style="font-weight:500;"><?php echo $results[0]->remarks; ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="6">
                            principle signature	:_____________________
                            <style></style>
                        </td>
                    </tr>
                </table>

                <div class="buttons" style="text-align:center;">

                    <a class="btn btn-default" href="{{route('admin.ResultMail',$resultid)}}" role="button">Send Mail</a>
                    <a class="btn btn-default" href="{{route('admin.ResultList',['exam'=>$results[0]->exam_id,'class'=>$results[0]->class_id])}}" role="button"><i class="fa fa-mail-reply"></i> Go Back</a>
                    <a class="btn btn-primary" onclick="printable()" href="#" role="button"> <i class="fa fa-print"></i> Print Result</a>
                </div>
            </div>
        </div><!--page-two ends-->
    @endif
    @endsection
@section('scripts')
    <script>
        function printable()
        {
            $( "body" ).addClass( "sidebar-collapse" );
            window.print();
            $( "body" ).removeClass( "sidebar-collapse" );
        }
    </script>
    @endsection