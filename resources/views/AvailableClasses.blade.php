@extends('layouts.app')
@section('title')
    Avalilable Classes <a href="{{route('admin.AddClass')}}" class="btn btn-default btn-md"> Add New Class </a>

@endsection
@section('content')
    <div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>S.n</th>
                <th>Class Name</th>
                <th>Section</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=1; ?>
            @foreach($classes as $class)

                <tr>
                    <td>{{$i}}</td>
                    <td>{{$class->class}}</td>
                    <td>{{$class->section}}</td>
                    <td><a href="{{route("admin.EditClass", $class->id)}}}}">Edit</a> |<a href="{{route("admin.DeleteClass", $class->id)}}"> Delete</a></td>

                </tr>
                <?php $i++; ?>
            @endforeach

            </tbody>

        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
