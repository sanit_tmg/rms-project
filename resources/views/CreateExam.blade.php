@extends('layouts.app')
@section('title','Add New Exam')
@section('content')
    <div class="create-form">
        <form class="form-horizontal" action="{{route('admin.CreateExam')}}" method="post">
            {{csrf_field()}}
            <div class="box-body">
                <div class="col-md-6">

                        <label class="col-md-3">Exam Name</label>
                        <div class="col-md-8">
                            <input class="form-control" placeholder="First Terminal Exam" type="text" name="exam_name" >
                        </div>
                </div>
                <div class="col-md-6">
                        <label class="col-md-3">Exam Date</label>
                        <div class="col-md-5">
                            <input class="form-control" type="date" name="exam_date" >
                        </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12" style="padding-top: 20px;">
                    <label class="col-md-2">Exam Notice</label>
                         <textarea name="description" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <div class="pull-right col-md-2 col-md-offset-1" style="padding-top: 20px;">
                    <button type="submit" class="btn btn-primary btn-lg">Add Exam</button>
                </div>
            </div>
        </form>
    </div>
@endsection