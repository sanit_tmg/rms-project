@extends('layouts.app')

@section('content')
    <h1>Welcome to <strong>School/College Management System</strong></h1>
    <div class="jumbotron">
        <p class="text-center">Hello, <span class="text-green">{{ Auth::user()->name }}</span><br/>

            @if (Session::has('flash_success'))
                <span class="fa fa-warning" style="color: red; margin-left: 50px;"></span>
                <span style="color: green;"> {{ Session::get('flash_success') }}</span>
            @else
                You have successfully logged into our system.
            @endif
        </p>

    </div>
@endsection
