@extends('layouts.app')

@section('content')
    @if(Session::has('flash_error'))
        <div class="jumbotron">
            <p class="text-center">
                <span style="color: red; font-weight: bold;"> Result Not Published Yet For This Examination </span>
            </p>
            <p class="text-center">
                <a href="{{route('admin.ResultForm',['exam'=>$examid,'class'=>$classid])}}" class="btn btn-info btn-lg">Add Result Now </a>
            </p>
        </div>
    @else
@section('title')
    Student Result List
    <a href="{{route('admin.ResultForm',['exam'=>$results[0]->exam_id,'class'=>$results[0]->class_id])}}" class="btn btn-default btn-md"> Add New Result </a>

@endsection
        <div class="box">
         <div class="box-body">
            <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <td>Roll no.</td>
                    <td>Name</td>
                    <td>Total</td>
                    <td>Percentage</td>
                    <td>Grade</td>
                    <td>Gpa</td>
                    <td>Result Date</td>
                    <td>Result Status</td>
                    <td>Remark</td>
                    <td>Status</td>
                    <td>Option</td>
                </tr>
                </thead>
                <input type="button" id="printPage" name="printPage" value="printPage">
                @foreach($results as $res)
                  <tr>

                    <td> {{$res->roll_no}}</td>
                    <td><a class="name" href="{{route('admin.ResultSheet',base64_encode($res->id))}}">
                            {{$res->name}}</a></td>
                    <td>
                        {{$res->total}}
                    </td>
                    <td> {{$res->percentage}}%</td>
                    <td> {{$res->grade}}</td>
                    <td> {{$res->gpa}}</td>
                    <td> {{$res->result_date}}</td>
                    <td> {{$res->result_status}}</td>
                    <td>{{strip_tags(substr($res->remarks,0,60))}}...</td>
                    <td>{{$res->is_active}}</td>
                    <td>
                        {{--<a href="{{route('admin.EditResult',$res->id)}}">Edit</a> ||--}}
                        <a href="{{route('admin.DeleteResult',$res->id)}}">Delete </a>
                    </td>
                </tr>
                    @endforeach
            </table>
             <a href="{{route('admin.EmailResult',['exam'=>$results[0]->exam_id,'class'=>$results[0]->class_id])}}" class="btn btn-default btn-md"> Email Results </a>

             <a href="{{route('allResult',['exam'=>$results[0]->exam_id,'class'=>$results[0]->class_id])}}" class="btn btn-primary"> <i class="fa fa-print"></i> Print Result</a>
         </div><!--page-two ends-->
        </div>
    @endif
@endsection
@section('scripts')
    <script>
        function printable()
        {
            $( "body" ).addClass( "sidebar-collapse" );
            window.print();
            $( "body" ).removeClass( "sidebar-collapse" );
        }

    </script>

    <script lang='javascript'>
        $(document).ready(function(){
            $('#printPage').click(function(){
                $( "body" ).addClass( "sidebar-collapse" );
                window.print();
            });
        });
    </script>
@endsection