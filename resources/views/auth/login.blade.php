@extends('auth.layouts.login')
@section('content')

    <div class="login-box-body">
        <div class="lockscreen-name">
            @if (Session::has('flash_error'))
                <span class="fa fa-warning" style="color: red; margin-left: 50px;"></span>
                <span style="color: red;"> {{ Session::get('flash_error') }}</span>
            @else
                <p class="login-box-msg">Sign in to start your session</p>
            @endif
        </div>
        <form action="{{url('login-attempt')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8 col-xs-push-2">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember_me"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
                <!-- /.col -->
        </form>
    </div>
    <!-- /.login-box-body -->


@endsection