<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Result Management System | Login</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
            <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
            <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/iCheck/square/blue.css')}}">
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<!-- Automatic element centering -->
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>School/College Management</b> Admin Panel</a>
    </div>

    @yield('content')



    <div class='lockscreen-footer text-center'>
        Copyright &copy; <?php echo date('Y');?> <br>
        <a href="www.mediawarner.com">warner Media / www.mediawarner.com</a>.
        <br>All rights reserved.
    </div>
</div><!-- /.center -->

<!-- jQuery 2.1.4 -->
<script type="text/javascript" src="{{asset('backend/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
        <!-- Bootstrap 3.3.2 JS -->
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>

</body>
</html>