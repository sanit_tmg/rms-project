@extends('layouts.app')
@section('title','Add New Class')
@section('content')
    <div class="create-form">
        <form class="form-horizontal" action="{{route('admin.CreateClass')}}" method="post">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label class="col-md-2">Class Name</label>
                    <div class="col-md-3">
                        <input class="form-control" type="text" name="className" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">Section</label>
                    <div class="col-md-3">
                        <input class="form-control" type="text" name="section" >
                    </div>
                </div>
                <div class="group product-save">
                    <button type="submit" class="btn btn-info">Add Class</button>
                </div>
            </div>
        </form>
    </div>
@endsection