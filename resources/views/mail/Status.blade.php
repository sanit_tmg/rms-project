@extends('layouts.app')
@section('title','Result Successful')
@section('content')
    <p style="text-align: center">
        <h2 style="color: #00a65a;">Result Sent Successfully</h2>
        <a class="btn btn-default" href="{{route('admin.ResultList',['exam'=>$exam,'class'=>$class])}}" role="button"><i class="fa fa-mail-reply"></i> Return Back</a>
    </p>

@endsection












