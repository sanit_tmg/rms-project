<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>

<table class="table" id="printableArea">
    <tr>
        <td colspan="6" style="letter-spacing: 3px; font-weight: bold; font-size: 20px;width:100%; text-align:center; border-top:none; border-bottom: 1px solid #ddd;"><h4>Academic Certificate   Class  {!! $class !!}
                '{{$section}}'<span style="color:blue"></span></h4></td>
    </tr>
    <tr>
        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Student Name:</td>
        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">{!! $name !!}</td>
        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Roll no.</td>
        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd;">{{$roll_no}}</td>
        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Exam Date:</td>
        <td colspan="1" style="border-top:none; border-bottom: 1px solid #ddd;">{{$result_date}}</td>
    </tr>
    <tr>
        <th style="border-top:none; border-bottom: 1px solid #ddd;">Sn.</th>
        <th style="border-top:none; border-bottom: 1px solid #ddd;">Subject</th>
        <th style="border-top:none; border-bottom: 1px solid #ddd;">Full Marks</th>
        <th style="border-top:none; border-bottom: 1px solid #ddd;">Pass Marks</th>
        <th style="border-top:none; border-bottom: 1px solid #ddd;">Obtained Marks</th>
        <th style="border-top:none; border-bottom: 1px solid #ddd;">Grade</th>
    </tr>

    <?php
    $count =1;
    $marks= json_decode($marks,true);
    ?>
    @for($i=0;$i<=sizeof($marks["Marks"])-1;$i++)

    <tr>
        <td style="border-top:none; border-bottom: 1px solid #ddd;">{!! $count !!}}</td>
        <td style="border-top:none; border-bottom: 1px solid #ddd;">{!! $marks["Marks"][$i]["Subject"]!!}</td>
        <td style="border-top:none; border-bottom: 1px solid #ddd;">{!! $marks["Marks"][$i]["FullMarks"]!!}</td>
        <td style="border-top:none; border-bottom: 1px solid #ddd;">{!! $marks["Marks"][$i]["PassMarks"]!!}</td>
        <td style="border-top:none; border-bottom: 1px solid #ddd;">{!! $marks["Marks"][$i]["ObtainedMarks"]!!}</td>
        <td style="border-top:none; border-bottom: 1px solid #ddd;">{!! $marks["Marks"][$i]["Grade"]!!}</td>
    </tr>

    <?php
    $count++;
    ?>

    @endfor

    <tr>
        <td style="border-top:none; border-bottom: 1px solid #ddd; padding-left:315px; font-weight:700;" colspan="4">Total</td>
        <td style="border-top:none; border-bottom: 1px solid #ddd;" colspan="2">{{$total}}</td>
    </tr>

    <tr>
        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;">Percentage</td>

        <td style="border-top:none; border-bottom: 1px solid #ddd;">{{$percentage}}%</td>

        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="1">GPA</td>

        <td style="border-top:none; border-bottom: 1px solid #ddd;" colspan="1">{{$gpa}}</td>

        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="1">Grade</td>

        <td style="border-top:none; border-bottom: 1px solid #ddd;" colspan="1">{{$grade}}</td>
    </tr>
    <tr>
        <td style="border-top:none; border-bottom: 1px solid #ddd; font-weight:700;" colspan="6">
            Remarks	:
            <br/>
            <p style="font-weight:500;">{!!  $remarks  !!}</p>
        </td>
    </tr>
</table>
</body>
</html>