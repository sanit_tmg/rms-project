@extends('layouts.app')

@section('title')
    All Examinations <a href="{{route('admin.AddExam')}}" class="btn btn-default btn-md"> Add New Exam </a>
@endsection
@section('content')
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>S.n</th>
                    <th>Exam Name</th>
                    <th>Exam Date</th>
                    <th>Exam Notice</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
                @foreach($exams as $exam)
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>{{$exam->name}}</td>
                        <td>{{$exam->exam_date}}</td>
                        <td>{{substr($exam->description,0,100)}}...</td>
                        <td><a href="{{route("admin.EditExam", $exam->id)}}}}">Edit</a> |<a href="{{route("admin.DeleteExam", $exam->id)}}"> Delete</a></td>
                    </tr>
                   <?php $i++; ?>
                @endforeach

                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
