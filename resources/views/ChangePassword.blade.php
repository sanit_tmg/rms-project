@extends('layouts.app')
@section('title','Settings')
@section('content')
    <div class="col-md-8">
    <form action="{{route('admin.change.password')}}" method="post">
        {{csrf_field()}}
        @if(session('msg'))
            <div class="box-body">
                <span class="help-block">
                    <strong class="red">{{session('msg')}}</strong>
                </span>
            </div>
        @endif
        <div class="box-body">
            <div class="form-group">
                <label class="col-sm-2">Current Email :</label>
                <label class="col-sm-5"> {{$email}}</label>
                <input type="hidden" name="id" value="{{$id}}">
            </div>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label class="col-sm-3">Type New Password</label>
                <input class="col-sm-5" type="password" name="newPassword">
            </div>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label class="col-sm-3">Confirm New Password</label>
                <input class="col-sm-5" type="password" name="confirmNewPassword">
            </div>
        </div>
        <div class="group">
            <button class="btn btn-info" type="submit">Change Password</button>
        </div>
    </form>
    </div>
@endsection