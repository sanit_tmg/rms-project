@extends('layouts.app')
@section('title','Add New User')
@section('content')
    <form class="form-horizontal" action="{{route('admin.StoreUser')}}" method="post">
        {{csrf_field()}}
        <div class="error-form">
            @if(session('errormsg'))
                <span class="help-block red">
                {{session('errormsg')}}
            </span>
            @endif
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="inputname" class="col-sm-3 ">Full Name</label>

                <div class="col-sm-9 {{$errors->has('fullname')? 'has->error':''}}">
                            <input type="text" class="form-control" id="inputname" name="fullname" placeholder="Full Name">
                    @if($errors->has('fullname'))
                        <span class="help-block">
                        {{$errors->first('fullname')}}
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="inputemail" class="col-sm-3">Email</label>

                <div class="col-sm-9 {{$errors->has('email')? 'has->error':''}}">
                    <input type="email" class="form-control" id="inputEmail3" name="email" placeholder="email">
                    @if($errors->has('email'))
                        <span class="help-block">
                        {{$errors->first('email')}}
                    </span>
                    @endif

                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword " class="col-sm-3">Password</label>

                <div class="col-sm-9 {{$errors->has('password')? 'has->error':''}}">
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    @if($errors->has('password'))
                        <span class="help-block">
                        {{$errors->first('password')}}
                    </span>
                    @endif
                    @if(session('msg'))
                        <span class=" error-message">
                        {{session('msg')}}
                    </span>
                    @endif
                </div>
            </div>
            {{--<div class="form-group">
                <label for="inputPassword" class="col-sm-3">Confirm Password</label>

                <div class="col-sm-9 {{$errors->has('confirmpassword')? 'has->error':''}}">
                    <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password">
                    @if($errors->has('confirmpassword'))
                        <span class="help-block">
                        {{$errors->first('confirmpassword')}}
                    </span>
                    @endif
                </div>
            </div>--}}
            <div class="form-group">
                <label for="selectAddress" class="col-md-3 ">Address </label>
                <div class="col-sm-9 {{$errors->has('address')? 'has->error':''}}">
                    <input type="text" class="form-control" name="address" id="address" placeholder="Address Here" >
                    @if($errors->has('address'))
                        <span class="help-block">
                        {{$errors->first('address')}}
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="selectAddress" class="col-md-3 ">Phone </label>
                <div class="col-sm-9 {{$errors->has('phone')? 'has->error':''}}">
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Here" >
                    @if($errors->has('phone'))
                        <span class="help-block">
                        {{$errors->first('phone')}}
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="selectusertype" class="col-md-3">User Role</label>
                <div class="col-md-9">
                    <select class="col-sm-offset-0 " name="usertype">
                        <option selected value="0">User</option>
                        <option value="1">Admin</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="group">
            <button type="submit" class="btn btn-info">Create User</button>
        </div>
        <!-- /.box-footer -->
    </form>
@endsection