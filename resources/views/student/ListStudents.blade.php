@extends('layouts.app')

@section('title')
    List Of Students <a href="{{route('admin.AddStudent')}}" class="btn btn-default btn-md"> Add New Student </a>
@endsection
@section('content')
    <div class="col-md-6"></div>
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th>Roll No</th>
                    <th>Name</th>
                    <th>Class</th>
                    <th>Section</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Parents_name</th>
                    <th>Parents_phone</th>
                    <th>Student_phone</th>
                    <th>Photo</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>

                @foreach($students as $student)
                    <tr>
                        <td>{{$student->roll_no}}</td>
                        <td>{{$student->name}}</td>
                        <td>{{$student->class}}</td>
                        <td>{{$student->section}}</td>
                        <td>{{substr($student->address,0,10)}}..</td>
                        <td>{{substr($student->email,0,10)}}..</td>
                        <td>{{$student->parents_name}}</td>
                        <td>{{substr($student->parents_phone,0,10)}}</td>
                        <td>{{substr($student->student_phone,0,10)}}</td>
                        @if($student->photo!='null')
                        <td><img src="{{asset('images/student/'.$student->photo)}}" alt="{{$student->name}}" class="img-responsive" ></td>
                        @else
                            <td><img src="{{asset('images/student/tech.jpg')}}" alt="{{$student->name}}" class="img-responsive" ></td>
                        @endif
                        <td><a href="{{route("admin.EditStudent", $student->id)}}">Edit</a> |<a href="{{route("admin.DeleteStudent", $student->id)}}"> Delete</a></td>
                    </tr>

                @endforeach

                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
