@extends('layouts.app')
@section('title','Add New Student')
@section('content')
        <div class="create-form">
            <form class="form-horizontal" action="{{route('admin.UpdateStudent',$student->id)}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label for="roll_no">Roll Number</label>
                                <input type="number" class="form-control" value="{{$student->roll_no}}" id="roll_no" placeholder="Enter Roll Number" name="roll_no">
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" value="{{$student->name}}" placeholder="Enter name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="class">Class</label>
                                <select name="class" class="form-control">
                                    @foreach($class as $cl)
                                        <option value="{{$cl->id}}">{{$cl->class}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="section">Section</label>
                                <select name="section" class="form-control">
                                    @foreach($class as $cl)
                                        <option value="{{$cl->section}}">{{$cl->section}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Address</label>
                                <input type="text" class="form-control" id="address" value="{{$student->address}}" placeholder="Enter Address" name="address">
                            </div>
                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="email" class="form-control" id="email" value="{{$student->email}}" placeholder="Enter Email" name="email">
                            </div>
                        </div>
                        <div class="col-md-3 col-md-offset-2">

                            <div class="form-group">
                                <label for="name">Parents Name</label>
                                <input type="text" class="form-control" id="parents_name" value="{{$student->parents_name}}" placeholder="Enter Parents Name" name="parents_name">
                            </div>
                            <div class="form-group">
                                <label for="name">Parents Phone</label>
                                <input type="text" class="form-control" id="parents_Phone" value="{{$student->parents_phone}}" placeholder="Enter Parents Phone" name="parents_phone">
                            </div>
                            <div class="form-group">
                                <label for="name">Students Phone</label>
                                <input type="number" class="form-control" id="student_phone" value="{{$student->student_phone}}" placeholder="Enter Students Phone" name="student_phone">
                            </div>
                            <div class="form-group">
                                <label for="image">Student's Photo</label>
                               <img src="{{asset('images/student/'.$student->photo)}}" class="img-thumbnail"/> <input type="file" name="image">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 col-md-offset-6">
                                <button type="submit" class="btn btn-primary btn-lg">Update Student</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection