@extends('layouts.app')
@section('title')
    Select Examination <a class="btn btn-default btn-md" href="{{route('admin.AddExam')}}" role="button">Add New Exam</a>
@endsection
@section('content')
        <div class="page-one">
            <div class="row-fluid">
                    <div class="page-one-content">
                        @foreach($exam as $ex)
                            <div class="col-md-4">
                                <a href="{{route('admin.ResultForm',['exam'=>$ex->id,$classid])}}">
                                    <div class="first">
                                        <h4>{{$ex->name}}</h4>
                                        <p>{{substr(strip_tags($ex->description),0,100)}}..</p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
            </div>
        </div><!--page-one ends-->

@endsection
