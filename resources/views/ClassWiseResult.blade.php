@extends('layouts.app')
@section('title')
    Select Exam  <a href="{{route('admin.AddExam')}}" class="btn btn-default btn-md"> Add New Exam </a>
    @endsection
@section('content')
    @if(Session::has('flash_error'))
        <div class="jumbotron">
            <p class="text-center">
                <span style="color: red; font-weight: bold;">{{session('flash_error')}}</span>
            </p>
            <p class="text-center">
                <a href="{{route('admin.CreateClassResult', $classid)}}" class="btn btn-info btn-lg ">Add Result Now </a>
            </p>
        </div>
        {{Session::forget('flash_error')}}
        @else
        <div class="page-one">
            <div class="row-fluid">
                    <div class="page-one-content">
                        @foreach($exam as $ex)
                        <div class="col-lg-4">
                            <a href="{{route('admin.ResultList',['exam'=>$ex->id,'class'=>$class])}}">
                                <div class="first">
                                    <h4>{{$ex->name}}</h4>
                                    <p>{{substr(strip_tags($ex->description),0,80)}}...</p>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
        </div><!--page-one ends-->
    @endif
@endsection
