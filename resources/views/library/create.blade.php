@extends('layouts.app')
@section('title','Add New Book')
@section('content')
    <div class="create-form">
        <form class="form-horizontal" action="{{route('admin.CreateStudent')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <div class="col-md-3 col-md-offset-1">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="roll_no">Book Number</label>
                            <input type="number" class="form-control" id="roll_no" placeholder="Enter Book Number" name="roll_no">
                        </div>
                        <div class="form-group">
                            <label for="roll_no">ISBN Number</label>
                            <input type="number" class="form-control" id="roll_no" placeholder="Enter ISBN Number" name="roll_no">
                        </div>
                        <div class="form-group">
                            <label for="name">Book Title</label>
                            <input type="text" required class="form-control" id="name" placeholder="Enter Book Title" name="name">
                        </div>
                        <div class="form-group">
                            <label for="name">Book Type</label>
                            <input type="text" required class="form-control" id="name" placeholder="Enter Book Type" name="name">
                        </div>
                        <div class="form-group">
                            <label for="name">Author Name</label>
                            <input type="text" required class="form-control" id="name" placeholder="Enter Author Name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="class">Quantity</label>
                            <input type="number" class="form-control">
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-md-offset-2">

                    <div class="form-group">
                        <label for="name">Edition</label>
                        <input type="text" required class="form-control" id="name" placeholder="Enter Edition" name="name">
                    </div>

                    <div class="form-group">
                        <label for="class">Price</label>
                        <input type="text" required class="form-control" id="name" placeholder="Enter Price" name="name ">
                    </div>

                    <div class="form-group">
                        <label for="name">Publisher Name</label>
                        <input type="text" class="form-control" id="parents_name" placeholder="Enter Publisher Name" name="parents_name">
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="col-md-4 col-md-offset-6">
                    <button type="submit" class="btn btn-primary btn-lg">Add Book</button>
                </div>
            </div>

    </div>
    </form>
    </div>
@endsection